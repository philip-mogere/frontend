import React from 'react'
import { Admin, Resource, ListGuesser, fetchUtils} from 'react-admin'
import jsonServerProvider from 'ra-data-json-server';
// import {CountyList} from '../../components/AdminLists/CountyList'


export const AdminPage = () => {

  const httpClient = (url, options = {}) => {
    if (!options.headers) {
        options.headers = new Headers({ Accept: 'application/json' });
    }
    options.header.set('Content-Range', 'county 0-20/20')
    options.headers.set('X-Custom-Header', 'foobar');
    return fetchUtils.fetchJson(url, options);
};

const dataProvider = jsonServerProvider('/county', httpClient);
  return (
    <Admin dataProvider={dataProvider}> 
      <Resource name="county" list={ListGuesser} >

      </Resource>
      <Resource name="caras" list={ListGuesser} >

      </Resource>
    </Admin>
  )
}
