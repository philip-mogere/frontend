import React from "react";
import "./countyPage.css";
import Sidebar from "../components/SideBar/Sidebar";
import Kenya from "../components/kenya/Kenya";

export default function CountyPage() {
  return (
    <div className="countyPage">
      <Sidebar />
      <div className="mapDiv text-center">
        <Kenya />
      </div>
    </div>
  );
}
