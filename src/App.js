import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import { Route, Switch } from "react-router-dom";
import TopBar from "./components/TopBar/TopBar";
import Footer from "./components/BottomNav/Footer";
// import Login from "./components/Login/Login";
import Budget from "./components/Budgets/Budget";
import Download from "./components/Downloads/Download";
import PoliticalDetails from "./components/CountyDetails/PoliticalDetails";
import CountyAssembly from "./components/CountyDetails/CountyAssembly";
import CountyPage from "./pages/CountyPage";
import CountyBudget from "./components/CountyDetails/CountyBudget";
import CountyBudgetChart from "./components/CountyDetails/CountyGCP";
import Subcounties from "./components/CountyDetails/Subcounties";
import Population from "./components/CountyDetails/Population";
import MarginalizedDetails from "./components/CountyDetails/MarginalizedDetails";
import Cara from "./components/CountyDetails/Cara";
import KenyainfoPage from "./components/KenyaInfo/KenyainfoPage";
import HealthData from "./components/CountyDetails/HealthData/HealthData";
import HealthStaff from "./components/CountyDetails/HealthData/HealthStaff";
import MarginalizedFirstPolicy from "./components/CountyDetails/Tables/MarginalizedFirstPolicy";
import CountryPopulation from "./components/KenyaInfo/CountryPopulation";
import CountyDisbursement from "./components/KenyaInfo/CountyDisbursement";
import Osr from "./components/KenyaInfo/Osr";
import CountryCara from "./components/KenyaInfo/CountryCara";
import { Container } from "react-bootstrap";
import DivisionOfRev from "./components/KenyaInfo/DivisionOfRev";
import Profile from "./components/CountyDetails/Profile";
import Socio_economic from "./components/KenyaInfo/Socio_economic";
import Pfm from "./components/KenyaInfo/Pfm";
import Equitable from "./components/KenyaInfo/Equitable";
import MarginalizedAreas from "./components/KenyaInfo/MarginalizedAreas";
import { CountyHome } from "./components/CountyDetails/CountyHome";
import { AdminPage } from "./pages/Admin/AdminPage";

function App() {
  return (
    <>
      <TopBar />
      <Container>
        <Switch>
          <Route exact path="/">
            <KenyainfoPage />
          </Route>
          <Route exact path="/login" component={AdminPage} />
          <Route exact path="/downloads" component={Download} />
          <Route exact path="/budgets" component={Budget} />

          <Route exact path="/county/:countyCode" component={CountyHome} />
          <Route
            exact
            path="/county/:countyCode/executive"
            component={PoliticalDetails}
          />
          <Route
            exact
            path="/county/:countyCode/budget"
            component={CountyBudget}
          />
          <Route
            exact
            path="/county/:countyCode/assembly"
            component={CountyAssembly}
          />
          <Route
            exact
            path="/county/:countyCode/subcounty"
            component={Subcounties}
          />
          <Route
            exact
            path="/county/:countyCode/marginalized"
            component={MarginalizedDetails}
          />
          <Route
            exact
            path="/county/:countyCode/population"
            component={Population}
          />
          <Route exact path="/county/:countyCode/cara" component={Cara} />
          <Route
            exact
            path="/county/:countyCode/gcp"
            component={CountyBudgetChart}
          />
          <Route exact path="/county/:countyCode/profile" component={Profile} />
          <Route exact path="/health" component={HealthData} />
          <Route exact path="/healthWorkers" component={HealthStaff} />
          <Route exact path="/marginalized" component={MarginalizedAreas} />
          <Route exact path="/counties" component={CountyPage} />

          <Route
            exact
            path="/marginalizedCounty"
            component={MarginalizedFirstPolicy}
          />
          <Route exact path="/population" component={CountryPopulation} />
          <Route exact path="/disbursements" component={CountyDisbursement} />
          <Route exact path="/enhancements" component={Osr} />
          <Route exact path="/cara" component={CountryCara} />
          <Route exact path="/dor" component={DivisionOfRev} />
          <Route exact path="/socio-economic" component={Socio_economic} />
          <Route exact path="/pfm" component={Pfm} />
          <Route path="equitable" component={Equitable} />
        </Switch>
      </Container>
      <Footer className="footer" />
    </>
  );
}

export default App;
