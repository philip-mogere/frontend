import React, {useState, useEffect} from 'react'

export default function Connection() {
   
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [county, setCounty ] = useState([]);
  
    useEffect(() => {
        const fetchMatches = async() =>{
        const response = await fetch("/county")
        .then(res => res.json())
        .then(
          (result) => {
            setIsLoaded(true);
            setCounty(result);
          },
          (error) => {
            setIsLoaded(true);
            setError(error);
          }
        )
        const data = await response.json();
        console.log(data);
        }
        fetchMatches()
    }, [])
  }
