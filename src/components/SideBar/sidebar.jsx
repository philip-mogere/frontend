import React, { useState, useEffect } from "react";
// import { Link } from 'react-router-dom'
import "./sidebar.css"
// import {MdHomeFilled} from 'react-icons/md';
// import { HiOutlineDocumentReport } from "react-icons/hi";
// import {FaFileDownload} from 'react-icons/fa'
// import {GiHealthNormal} from 'react-icons/gi'
// import {GiDesert} from 'react-icons/gi'
// import {IoIosPeople} from 'react-icons/io'


export default function Sidebar() {

    const [counties, setCounties] = useState({});
    const Counties = Array.from(counties)
    useEffect(() => {
        fetchCounties();
            // eslint-disable-next-line react-hooks/exhaustive-deps

      }, []);
      const fetchCounties = async () => {
        const data = await fetch(`/county/`);
        const dataJson = await data.json();
        setCounties(dataJson);
      };
    return (
        <div className = "sidebar active">
            <div className="sidebarWrapper">
                {/* <div className="sidebarDashboardMenu">
                
                     <h3 className="sidebarTitle">Dashboard</h3>
                     <ul className="sidebarList">
                         <li className="sidebarListItem">
                             <MdHomeFilled/> &nbsp;<Link to="/">Home</Link>
                         </li>
                         <li className="sidebarListItem">
                            <FaFileDownload/>&nbsp;<Link to="/downloads">Downloads</Link>
                         </li>
                         <li className="sidebarListItem">
                            <HiOutlineDocumentReport/>&nbsp; 
                            <Link to="/budgets">Financial Data </Link>
                         </li>
                         <li className="sidebarListItem">
                            <GiDesert/>&nbsp; 
                            <Link to="/marginalized">Marginalized Areas </Link>
                         </li>
                         <li className="sidebarListItem">
                            <IoIosPeople/>&nbsp; 
                            <Link to="/socio-economic">Demographic data </Link>
                         </li>
                     </ul>
                     <hr></hr>
                </div> */}
                <h4 className="sidebarTitle">County list</h4>
                <div className="sidebarCountyMenu">
                     
                     <ul className="sidebarList">
                         {Counties.map(county => (
                            <a href={`/county/${county.countyCode}`} key={county.countyCode}> <li className="sidebarListItem"><strong >
                                {county.countyCode<10? "00" + county.countyCode : "0"+county.countyCode}-{county.countyName}</strong> </li></a>
                         ))}
                         
                      </ul>
                </div>
            </div>
        </div>
        
    )
}
