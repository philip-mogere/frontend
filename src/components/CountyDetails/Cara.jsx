import React from 'react'
import { useEffect } from 'react'
import { useState } from 'react'
import NavBar from '../NavBar/NavBar'
import './Cara.css'

export default function Cara(county) {

    const [sentCounty, setSentCounty]= useState({
        cara:[]
    })

    const caraArray=Array.from(sentCounty.cara)
    const code = county.match.params.countyCode

    useEffect(()=>{
        fetchCara()
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[])

    const fetchCara = async() =>{
        const data = await fetch(`/county/${code}`)
        const dataJson = await data.json()
        setSentCounty(dataJson)
    }
    
    function numberWithCommas(x) {
        return x.toLocaleString("en-US");
    }

    return (
        <div className="caraContent">
            <NavBar county={county.match}/>   
            <div className="cara">
                    <h1 className='detailsInfo'>County Ceilings in Millions of KSH</h1> 
                    <table className='detailsInfo'>
                        <th>Year</th><th>County Ceiling</th>
                        {caraArray.map(cara =>(<tr>
                        <td>{cara.year}</td>
                        <td>{numberWithCommas(cara.ceiling)}</td>
                       </tr>))
                       }                      
                    </table>  
                               
            </div>
        </div>
    )
}
                    

