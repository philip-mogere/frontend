import Mombasa from './coat of arms/Mombasa.png'
import Kwale from './coat of arms/Kwale.jpeg'
import Kilifi from './coat of arms/Kilifi.png'
import Tana  from './coat of arms/Tana River.jpeg'
import Lamu  from './coat of arms/Lamu.jpeg'
import Taveta  from './coat of arms/Taveta.jpeg'
import Garissa from './coat of arms/Garissa.png'
import Wajir  from './coat of arms/Wajir.jpeg'
import Mandera from './coat of arms/Mandera.jpeg'
import Marsabit  from './coat of arms/Marsabit.jpeg'
import Isiolo  from './coat of arms/Isiolo.jpeg'
import Meru  from './coat of arms/Meru.png'
import Tharaka  from './coat of arms/Tharaka.png'
import Embu  from './coat of arms/Embu.png'
import Kitui  from './coat of arms/Kitui.jpeg'
import Machakos  from './coat of arms/Machakos.png'
import Makueni  from './coat of arms/Makueni.jpeg'
import Nyandarua  from './coat of arms/Nyandarua.jpeg'
import Nyeri  from './coat of arms/Nyeri.jpeg'
import Kirinyaga  from './coat of arms/Kirinyaga.png'
import Muranga  from './coat of arms/Muranga.png'
import Kiambu  from './coat of arms/Kiambu.jpeg'
import Turkana  from './coat of arms/Turkana.png'
import Pokot  from './coat of arms/Pokot.jpeg'
import Samburu  from './coat of arms/Samburu.jpeg'
import Trans  from './coat of arms/Trans nzoia.jpeg'
import Uasin  from './coat of arms/Uasingishu.jpeg'
import Elgeyo  from './coat of arms/Marakwet.jpeg'
import Nandi  from './coat of arms/Nandi.jpeg'
import Baringo from './coat of arms/Baringo.jpeg'
import Laikipia from './coat of arms/Laikipia.jpeg'
import Nakuru from './coat of arms/Nakuru.jpeg'
import Narok from './coat of arms/Narok.png'
import Kajiado from './coat of arms/Kajiado.jpeg'
import Kericho from './coat of arms/Kericho.png'
import Bomet from './coat of arms/Bomet.jpeg'
import Kakamega from './coat of arms/Kakamega.jpeg'
import Vihiga from './coat of arms/Vihiga.jpeg'
import Bungoma from './coat of arms/Bungoma.jpeg'
import Busia from './coat of arms/Busia.jpeg'
import Siaya from './coat of arms/Siaya.png'
import Kisumu from './coat of arms/Kisumu.jpeg'
import Homabay from './coat of arms/Homabay.jpeg'
import Migori from './coat of arms/Migori.jpeg'
import Kisii from './coat of arms/Kisii.png'
import Nyamira from './coat of arms/Nyamira.jpeg'
import Nairobi from './coat of arms/Nairobi.png'




export const CoatOfArms=[
    Mombasa,
    Kwale,
    Kilifi, 
    Tana,
    Lamu,
    Taveta,
    Garissa,
    Wajir, 
    Mandera,
    Marsabit,
    Isiolo,
    Meru,
    Tharaka,
    Embu,
    Kitui,
    Machakos,
    Makueni,
    Nyandarua,
    Nyeri,
    Kirinyaga,
    Muranga,
    Kiambu,
    Turkana,
    Pokot,
    Samburu,
    Trans,
    Uasin,
    Elgeyo,
    Nandi,
    Baringo,
    Laikipia,
    Nakuru,
    Narok,
    Kajiado,
    Kericho,
    Bomet,
    Kakamega,
    Vihiga,
    Bungoma,
    Busia,
    Siaya,
    Kisumu,
    Homabay,
    Migori,
    Kisii,
    Nyamira,
    Nairobi
]