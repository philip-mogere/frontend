import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import NavBar from "../NavBar/NavBar";
import "./countyAssembly.css";
import { PotraitsSpeaker } from "./PortaitSpeaker";
import { PotraitsDSpeaker } from "./PotraitsDSpeaker";
import { PotraitsMajority } from "./PotraitsMajority";
import { PotraitsMinority } from "./PotraitsMinority";
import { PotraitsClerk } from "./PotraitsClerk";

export default function CountyAssembly(county) {
  const [assembly, setAssembly] = useState({
    mcas: {},
  });
  const code = county.match.params.countyCode;
  const mcas = Array.from(assembly.mcas);

  useEffect(() => {
    fetchAssembly();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fetchAssembly = async () => {
    const data = await fetch(`/county/${code}/assembly`);
    const dataJson = await data.json();
    setAssembly(dataJson);
  };

  return (
    <div className="content">
      <NavBar county={county.match} />
      <div className="assembly">
        <h3>County Assembly Leadership</h3>
        <hr />
        <div className="speaker text-center">
          <div className="mb-3">
            {PotraitsSpeaker[code - 1] ? (
              <img
                className="coa mb-3"
                alt="County Deputy Governor"
                src={PotraitsSpeaker[code - 1]}
              />
            ) : (
              <img
                className="coa mb-3"
                alt="County Deputy Governor"
                src={PotraitsSpeaker[PotraitsSpeaker.length - 1]}
              />
            )}

            <h4 className="detailsInfo">Speaker</h4>
            <h4 className="detailsInfo"> Hon. {assembly.speaker}</h4>
          </div>
          <hr />
          <div>
            {PotraitsDSpeaker[code - 1] ? (
              <img
                className="coa mb-3"
                alt="County Deputy Governor"
                src={PotraitsDSpeaker[code - 1]}
              />
            ) : (
              <img
                className="coa mb-3"
                alt="County Deputy Governor"
                src={PotraitsSpeaker[PotraitsSpeaker.length - 1]}
              />
            )}

            <h4 className="detailsInfo">Deputy Speaker</h4>
            <h4 className="detailsInfo"> Hon. {assembly.deputySpeaker}</h4>
          </div>
        </div>
        <hr />
        <div className="officials d-flex">
          <div>
            {PotraitsMajority[code - 1] ? (
              <img
                className="coa mb-3"
                alt="Majority Leader"
                src={PotraitsMajority[code - 1]}
              />
            ) : (
              <img
                className="coa mb-3"
                alt="County Assembly clerk"
                src={PotraitsSpeaker[PotraitsSpeaker.length - 1]}
              />
            )}
            <h4 className="detailsInfo">Majority Leader</h4>
            <h4 className="detailsInfo"> Hon. {assembly.majorityLeader}</h4>
          </div>
          <div>
            {PotraitsMinority[code - 1] ? (
              <img
                className="coa mb-3"
                alt="Minority Leader"
                src={PotraitsMinority[code - 1]}
              />
            ) : (
              <img
                className="coa mb-3"
                alt="County Assembly clerk"
                src={PotraitsMinority[PotraitsMinority.length - 1]}
              />
            )}
            <h4 className="detailsInfo">Minority Leader</h4>
            <h4 className="detailsInfo"> Hon. {assembly.minorityLeader}</h4>
          </div>
          <div>
            {PotraitsClerk[code - 1] ? (
              <img
                className="coa mb-3"
                alt="County Assembly clerk"
                src={PotraitsClerk[code - 1]}
              />
            ) : (
              <img
                className="coa mb-3"
                alt="County Assembly clerk"
                src={PotraitsClerk[PotraitsClerk.length - 1]}
              />
            )}

            <h4 className="detailsInfo">County Assembly Clerk</h4>
            <h4 className="detailsInfo"> Hon. {assembly.clerk}</h4>
          </div>
        </div>
        <table>
          <thead>
            <tr>
            <th>Members of County Assembly</th>
            <th>Ward</th>
            </tr>
          </thead>
          <tbody>
            {mcas.map((mca)=>(
              <tr key={mca.id}>
                <td>Hon. {mca.name}</td><td>{mca.ward} Ward</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}
