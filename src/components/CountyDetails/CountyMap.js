import Mombasa from './Counties/Mombasa.svg'
import Kwale from './Counties/Kwale.svg'
import Kilifi from './Counties/Kilifi.svg'
import Tana from './Counties/Tana River.svg'
import Lamu from './Counties/Lamu.svg'
import Taita from './Counties/Taita Taveta.svg'
import Garissa from './Counties/Garissa.svg'
import Wajir from './Counties/Wajir.svg'
import Mandera from './Counties/Mandera.svg'
import Marsabit from './Counties/Marsabit.svg'
import Isiolo from './Counties/Isiolo.svg'
import Meru from './Counties/Meru.svg'
import Tharaka from './Counties/Tharaka-Nithi.svg'
import Embu from './Counties/Embu.svg'
import Kitui from './Counties/Kitui.svg'
import Machakos from './Counties/Machakos.svg'
import Makueni from './Counties/Makueni.svg'
import Nyandarua from './Counties/Nyandarua.svg'
import Nyeri from './Counties/Nyeri.svg'
import Kirinyaga from './Counties/Kirinyaga.svg'
import Muranga from "./Counties/Murang'a.svg"
import Kiambu from './Counties/Kiambu.svg'
import Turkana from './Counties/Turkana.svg'
import Pokot from './Counties/West Pokot.svg'
import Samburu from './Counties/Samburu.svg'
import Nzoia from './Counties/Trans Nzoia.svg'
import Uasin from './Counties/Uasin Gishu.svg'
import Marakwet from './Counties/Elgeyo Marakwet.svg'
import Nandi from './Counties/Nandi.svg'
import Baringo from './Counties/Baringo.svg'
import Laikipia from './Counties/Laikipia.svg'
import Nakuru from './Counties/Nakuru.svg'
import Narok from './Counties/Narok.svg'
import Kajiado from './Counties/Kajiado.svg'
import Kericho from './Counties/Kericho.svg'
import Bomet from './Counties/Bomet.svg'
import Kakamega from './Counties/Kakamega.svg'
import Vihiga from './Counties/Vihiga.svg'
import Bungoma from './Counties/Bungoma.svg'
import Busia from './Counties/Busia.svg'
import Siaya from './Counties/Siaya.svg'
import Kisumu from './Counties/Kisumu.svg'
import Homabay from './Counties/Homa Bay.svg'
import Migori from './Counties/Migori.svg'
import Kisii from './Counties/Kisii.svg'
import Nyamira from './Counties/Nyamira.svg'
import Nairobi from './Counties/Nairobi.svg'


export const CountyMap=[
    Mombasa,
    Kwale,
    Kilifi,
    Tana,
    Lamu,
    Taita,
    Garissa,
    Wajir,
    Mandera,
    Marsabit,
    Isiolo,
    Meru,
    Tharaka,
    Embu,
    Kitui,
    Machakos,
    Makueni,
    Nyandarua,
    Nyeri,
    Kirinyaga,
    Muranga,
    Kiambu,
    Turkana,
    Pokot,
    Samburu,
    Nzoia,
    Uasin,
    Marakwet,
    Nandi,
    Baringo,
    Laikipia,
    Nakuru,
    Narok,
    Kajiado,
    Kericho,
    Bomet,
    Kakamega,
    Vihiga,
    Bungoma,
    Busia,
    Siaya,
    Kisumu,
    Homabay,
    Migori,
    Kisii,
    Nyamira,
    Nairobi    
]
