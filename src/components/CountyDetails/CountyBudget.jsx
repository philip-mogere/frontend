import React from "react";
import { useEffect, useState } from "react";
import "./countyBudget.css";
import NavBar from "../NavBar/NavBar";
import { CountyBudgetColumns, AllocationColumns } from "./Tables/Columns";
import Table from "./Tables/Table";

export default function CountyBudget(county) {
  const [sentCounty, setBudget] = useState({
    countyBudget: [],
    countyDisbursement: {
      disbursement: 0,
      period: "",
    },
  });
  const [allocations, setAllocations] = useState({});

  const budgets = Array.from(sentCounty.countyBudget);
  const code = county.match.params.countyCode;
  const allocation = Array.from(allocations);

  useEffect(() => {
    fetchBudget();
    fetchAllocations();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fetchBudget = async () => {
    const data = await fetch(`/county/extended/${code}`);
    const dataJson = await data.json();
    setBudget(dataJson);
  };
  const fetchAllocations = async () => {
    const data = await fetch(`/county/${code}/allocation`);
    const dataJson = await data.json();
    setAllocations(dataJson);
  };

  return (
    <div className="content">
      <NavBar county={county.match} className="navBar" />
      <div className="budget">
        <h1 className="detailsInfo text-center">
          Breakdown of financial data{" "}
        </h1>
        <div className="chartInput">
          <h3>County Allocations</h3>
          <Table Columns={AllocationColumns} Data={allocation} />
          <p></p>
          <h3>County Budget Overview</h3>
          <Table Columns={CountyBudgetColumns} Data={budgets} />
        </div>
      </div>
    </div>
  );
}
