import React, { useEffect, useState } from "react";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Legend } from "recharts";
import NavBar from "../NavBar/NavBar";
import "./countyBudget.css";

export default function CountyBudgetChart(props) {
  const [sentCounty, setCounty] = useState({
    countyGCPs: {},
  });
  const code = props.match.params.countyCode;

  useEffect(() => {
    fetchCounty();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const gcps = Array.from(sentCounty.countyGCPs);
  const fetchCounty = async () => {
    const data = await fetch(`/county/extended/${code}`);
    const dataJson = await data.json();
    setCounty(dataJson);
  };

  return (
    <div className="content">
      <NavBar county={props.match} />
      <div className="chart">
        <h3>County GCP per sector in Millions (Ksh) </h3>

        <table>
          <tr>
            <th>miningAndQuarrying</th>
            <th>manufacturing</th>
            <th>electricitySupply</th>
            <th>waterSupplyWaste</th>
            <th>construction</th>
            <th>wholesaleAndRetailTrade</th>
            <th>transportAndStorage</th>
            <th>
            realEstateActivities
            </th>
            <th>education</th>
            <th>otherServiceActivities</th>
            <th>agricultureForestryAndFishing</th>
            <th>accomodationAndFoodServices</th>
          </tr>
             
          {gcps.map((gcp) => (
                <tr key="id">
                  {" "}
                  <td>{gcp.miningAndQuarrying}</td><td>{gcp.manufacturing} </td>
                  <td>{gcp.electricitySupply} </td><td>{gcp.waterSupplyWaste} </td>
                  <td>{gcp.construction}</td><td>{gcp.wholesaleAndRetailTrade} </td>
                  <td>{gcp.transportAndStorage}</td><td>{gcp.realEstateActivities} </td>
                  <td>{gcp.education}</td><td>{gcp.otherServiceActivities} </td>
                  <td>{gcp.agricultureForestryAndFishing}</td><td>{gcp.accomodationAndFoodServices} </td>
                </tr>))}
          
        </table>

        {/* <ResponsiveContainer width="83%" aspect={4/2.5}> */}
        <BarChart width={1000} height={650} data={gcps}>
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Bar dataKey="accomodationAndFoodServices" fill="#8884d8" />
          <Bar dataKey="Education" fill="#82ca9d" />
          <Bar dataKey="construction" fill="#f00000" />
          <Bar dataKey="agricultureForestryAndFishing" fill="#60c018" />
          <Bar dataKey="electricitySupply" fill="#ffc000" />
          <Bar dataKey="financialAndInsuranceActivities" fill="#cbe4aa" />
          <Bar dataKey="humanHealthAndSocialwork" fill="#f03018" />
          <Bar dataKey="informationAndCommunication" fill="#180000" />
          <Bar dataKey="manufacturing" fill="#f5b2b0" />
          <Bar dataKey="miningAndQuarrying" fill="#5f4b1e" />
          <Bar dataKey="otherServiceActivities" fill="#d6b62b" />
          <Bar dataKey="professionalTechnicalAndSupport" fill="#323552" />
          <Bar dataKey="publicAdministrationAndDefence" fill="#caf50b" />
          <Bar dataKey="realEstateActivities" fill="#ee8732" />
          <Bar dataKey="transportAndStorage" fill="#e966b7" />
          <Bar dataKey="waterSupplyWaste" fill="#1740b0" />
          <Bar dataKey="wholesaleAndRetailTrade" fill="#1740b0" />
          <Legend />
        </BarChart>
        {/* </ResponsiveContainer> */}
      </div>
    </div>
  );
}
