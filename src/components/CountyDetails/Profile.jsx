import React ,{useState, useEffect}from 'react'
import { useParams, useLocation } from 'react-router';
import { Potraits } from "./Potraits";

export default function Profile() {
    const code = useParams().countyCode
    const location = useLocation().pathname.split(1);
    const [executive, setExecutive] = useState({  
      });
    const [county, setCounty] = useState({});

    
      useEffect(() => {
        fetchExecutive();
        fetchCounty();
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, []);
    
      const fetchExecutive = async () => {
        const data = await fetch(`/county/${code}/executive`);
        const dataJson = await data.json();
        setExecutive(dataJson);
      };
      const fetchCounty = async () => {
          const data = await fetch(`/county/${code}`);
          const dataJson = await data.json();
          setCounty(dataJson);
        };

    return (
        <div className="profile text-center mt-3">
            <h2>{county.countyName} County</h2>
            {location ?
            <>
            <h3> Governor  </h3>
              <hr />
              <img src={Potraits[code - 1]} alt="Governor" />
              <p>Governor {executive.governor}</p>
              <h4>Profile</h4>
              <p>{executive.governorProfie}</p>
              </> 
              : 
              <>
              {/* <h3> Deputy Governor  </h3>
              <hr />
              <img src={Potraits[code - 1]} alt="Governor" />
              <p>Governor {executive.governor}</p>
              <h4>Profile</h4>
              <p>{executive.governorProfie}</p> */}
              </>
            }
            
            
        </div>
    )
}
