// import { Link } from 'react-router-dom'
import React from "react";
import { useEffect, useState } from "react";
import "./countyDetail.css";
import { Container, Row, Col, Card, ListGroup } from "react-bootstrap";
import BusinessIcon from "@material-ui/icons/Business";
import AlternateEmailIcon from "@material-ui/icons/AlternateEmail";
import LanguageIcon from "@material-ui/icons/Language";
import ContactPhoneIcon from "@material-ui/icons/ContactPhone";
import ContactMailIcon from "@material-ui/icons/ContactMail";
import HomeWorkIcon from "@material-ui/icons/HomeWork";
import { CoatOfArms } from "./CoatOfArms";
import { CountyMap } from "./CountyMap";


export const CountyDetail = ({ match }) => {
  const [county, setCounty] = useState({});
  // const [subCountyList, setSubCountyList] = useState({});
  const [MunicipalityList, setMunicipalityList] = useState({});

  // const subCounties = Array.from(subCountyList);
  const municipalities = Array.from(MunicipalityList);

  useEffect(() => {
    fetchCounty();
    // fetchSubCounties();
    fetchMunicipalities();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const code = match.params.countyCode;
  const name = county.countyName;
  const fetchCounty = async () => {
    const data = await fetch(`/county/extended/${code}`);
    const dataJson = await data.json();
    setCounty(dataJson);
  };

  // const fetchSubCounties = async () => {
  //   const data = await fetch(`/county/${code}/subcounty`);
  //   const dataJson = await data.json();
  //   setSubCountyList(dataJson);
  // };

  const fetchMunicipalities = async () => {
    const data = await fetch(
      `/county/${code}/municipality`
    );
    const dataJson = await data.json();
    setMunicipalityList(dataJson);
  };

  return (
    <div>
      <Container style={{ marginTop: "60px" }}>
        <Row className="text-center">
          <Col className=" card-col ">
            <Col style={{ paddingLeft: "3rem" }}>
              <img
                variant="top"
                alt="Coat of arms"
                width={171}
                height={180}
                src={CoatOfArms[code - 1]}
              />
            </Col>
            <Col>
              <Card className="card">
                <ListGroup className="card-list " variant="flush">
                  <ListGroup.Item className="detailsInfo">
                    <HomeWorkIcon className="detailsIcon" /> Headquarters -{" "}
                    {county.countyHQ} Town
                  </ListGroup.Item>
                  <ListGroup.Item className="detailsInfo">
                    <BusinessIcon className="detailsIcon" /> {county.offices}
                  </ListGroup.Item>
                  <ListGroup.Item className="detailsInfo">
                    <ContactMailIcon className="detailsIcon" /> Address -{" "}
                    {county.address}
                  </ListGroup.Item>
                  <ListGroup.Item className="detailsInfo">
                    <AlternateEmailIcon className="detailsIcon" /> email-{" "}
                    {county.email}
                  </ListGroup.Item>
                  <ListGroup.Item className="detailsInfo">
                    <LanguageIcon className="detailsIcon" /> Website -{" "}
                    <a href={county.website}>{county.website}</a>
                  </ListGroup.Item>
                  <ListGroup.Item className="detailsInfo">
                    <ContactPhoneIcon className="detailsIcon" /> Phone Number -{" "}
                    {county.phone}
                  </ListGroup.Item>
                </ListGroup>
              </Card>
            </Col>
          </Col>
          <Row>
            <Col>
            <p></p>
              <h3>Map of {name} County</h3>
              <img src={CountyMap[code-1]} 
              alt={name}
              width={1000}
              height={1000} 
              />
              <hr />
            </Col>
          </Row>
          <Row className="subcounties">
           

            <Col>
              {municipalities.length > 1 ? (
                <table>
                  <thead>
                    <tr>
                      <th>Municipality</th>
                      <th>Status</th>
                      <th>Population</th>
                    </tr>
                  </thead>
                  <tbody>
                    {municipalities.map((municipality) => (
                      <tr key={municipality.id}>
                        <td>{municipality.municipality}</td>
                        <td>{municipality.status} </td>
                        <td>{municipality.population} </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              ) : (
                <></>
              )}
            </Col>
          </Row>
        </Row>
      </Container>
    </div>
  );
};
