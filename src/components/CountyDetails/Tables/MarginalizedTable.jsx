import React, { useEffect, useMemo, useState } from "react";
import { useTable, useGlobalFilter, usePagination } from "react-table";
import { MarginalizedColumns } from "./Columns";
import { MarginalizationAddendumColumns } from "./Columns";
import GlobalFilter from "./GlobalFilter";
import { Button } from "react-bootstrap";
import Table from "./Table";

export default function MarginalizedTable() {
  const [marginalizedAreas, setMarginalizedAreas] = useState([]);
  const [marginalizedAddendum, setMarginalizedAddendum] = useState([]);

  useEffect(() => {
    fetchMarginalizedAreas();
    fetchMarginalizedAddendum();
  }, []);

  const fetchMarginalizedAreas = async () => {
    const data = await fetch(`/county/marginalized/`);
    const dataJson = await data.json();
    setMarginalizedAreas(dataJson);
  };

  const fetchMarginalizedAddendum = async () => {
    const data = await fetch(
      `/county/marginalization-addendum`
    );
    const dataJson = await data.json();
    setMarginalizedAddendum(dataJson);
  };
  const columns = useMemo(() => MarginalizedColumns, []);
  const data = useMemo(() => marginalizedAreas);

  const tableInstance = useTable(
    {
      columns,
      data,
    },
    useGlobalFilter,
    usePagination
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    nextPage,
    previousPage,
    canNextPage,
    canPreviousPage,
    prepareRow,
    gotoPage,
    pageCount,
    pageOptions,
    setPageSize,
    state,
    setGlobalFilter,
  } = tableInstance;

  const { globalFilter, pageIndex, pageSize } = state;

  return (
    <div>
      <h3 className="text-center ">
        Marginalized Areas According to the 2nd Policy
      </h3>
      <h3 className="text-center mt-5">
        Marginalized Areas According to the 2nd Policy
      </h3>
      <p className="text-center">
        The Second Policy, done in 2017, covered a period of seven financial
        years. The Policy will be used to share revenues for FYs 2011/12;
        2012/13; 2013/14; 2017/18; 2018/19, 2019/20; 2020/21.{" "}
      </p>

      <GlobalFilter filter={globalFilter} setFilter={setGlobalFilter} />
      <table {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps()}>{column.render("Header")}</th>
              ))}
            </tr>
          ))}
        </thead>

        <tbody {...getTableBodyProps()}>
          {page.map((row) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map((cell) => {
                  return (
                    <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
      <div className="text-center">
        <span>
          Page{""}
          <strong>
            {pageIndex + 1} of {pageOptions.length}
          </strong>
          {"  "}
        </span>
        <span>
          |Go to Page:
          <input
            type="number"
            defaultValue={pageIndex + 1}
            onChange={(e) => {
              const pageNumber = e.target.value
                ? Number(e.target.value) - 1
                : 0;
              gotoPage(pageNumber);
            }}
            style={{ width: "50px" }}
          />
        </span>
        <select
          value={pageSize}
          onChange={(e) => setPageSize(Number(e.target.value))}
        >
          {[50, 100, 200].map((pageSize) => (
            <option key={pageSize} value={pageSize}>
              Show {pageSize}
            </option>
          ))}
        </select>
        {"  "}
        <Button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
          {"<<"}
        </Button>
        {"  "}
        <Button onClick={() => previousPage()} disabled={!canPreviousPage}>
          Previous
        </Button>
        {"  "}
        <Button onClick={() => nextPage()} disabled={!canNextPage}>
          Next
        </Button>
        {"  "}
        <Button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
          {">>"}
        </Button>
      </div>

      <h3 className="mt-5"> Addendum to the Second Policy</h3>
      <p className="text-center p-3">
        The Commission prepared an addendum to the Second policy. The addendum
        amends the Second Policy to share Fund allocations for financial years
        2011/12, 2012/13 and 2013/14, amounting to Ksh. 7.64 billion with the 14
        counties identified under the First policy as marginalised.{" "}
      </p>
      <Table
        Columns={MarginalizationAddendumColumns}
        Data={marginalizedAddendum}
      />
    </div>
  );
}
