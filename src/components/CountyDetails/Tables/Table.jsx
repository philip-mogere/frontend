import React, { useMemo } from "react";
import { useTable, useGlobalFilter, useRowSelect } from "react-table";
import GlobalFilter from "./GlobalFilter";
import {Checkbox} from './Checkbox'
import './Table.css'

export default function Table(props) {

  const {Columns, Data} = props;
  // const [selectedRows, setSelectedRows] = useState([])
  const columns = useMemo(() => Columns, []);
  const data = useMemo(() => Data);


  const tableInstance = useTable({
    columns,
    data,
  }, useGlobalFilter, 
  useRowSelect,
  // (hooks)=>{
  //   hooks.visibleColumns.push((columns)=>{
  //     return[
  //       {
  //         // id:'Selection',
  //         // Header:'Select Rows', 
  //         // Cell:({row})=>(
  //         //   <Checkbox {...row.getToggleRowSelectedProps()}/>
  //         // )
  //       }, 
  //       ...columns
  //     ]
  //   })
  // }
  );
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    selectedFlatRows,
    state,
    setGlobalFilter,
    allColumns,
  } = tableInstance;

  // const Selection = JSON.stringify(selectedFlatRows: selectedFlatRows.map((row)=> row.original))
  // console.log(Selection);
  const { globalFilter } = state;
  return (
    <div >
    <div className="d-flex">
      
      {
        allColumns.map(column =>(
          <div key={column.id}>
            <label>
              <input type='checkbox' {...column.getToggleHiddenProps()}/>
              {column.Header}
            </label>
          </div>
        ))
      }
    </div>
    <div>
      <GlobalFilter filter={globalFilter} setFilter={setGlobalFilter} />
      <table {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps()}>{column.render("Header")}</th>
              ))}
            </tr>
          ))}
        </thead>

        <tbody {...getTableBodyProps()}>
          {rows.map((row) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map((cell) => {
                  return (
                    <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
      </div>
    </div>
  );
}
