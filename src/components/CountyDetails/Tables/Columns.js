export const BudgetColumns = [
    {
      header: "County Name",
      accessor: "countyName",
    },
    {
      Header: "Recurrent",
      accessor: "recurrent",
      Cell: props => numberWithCommas(props.value) 
    },
    {
      Header: "Development",
      accessor: "development",
      Cell: props => numberWithCommas(props.value) 
    },
   
  ];

  export const CountyBudgetColumns = [
    {
      header: "Year",
      accessor: "year",
    },
    {
      Header: "Recurrent",
      accessor: "recurrent",
      Cell: props => numberWithCommas(props.value) 
    },
    {
      Header: "Development",
      accessor: "development",
      Cell: props => numberWithCommas(props.value) 
    },
    {
      Header: "Equitable Share",
      accessor: "equitableShare",
      Cell: props => numberWithCommas(props.value) 
    },
    {
      Header: "Own Revenue",
      accessor: "ownRevenue",
      Cell: props => numberWithCommas(props.value) 
    },
  ];
  
  export const HealthDataCOLUMNS =[
      {
          Header: 'County',
          accessor:'countyName'
      },
      {
          Header: 'Bed Density',
          accessor:'bedDensity'
      },
      {
          Header: 'Insurance Coverage',
          accessor: 'insuranceCoverage'
      },
      {
          Header:'Out patient Expenditure',
          accessor:'outptientExpenditure',
          Cell: props => numberWithCommas(props.value) 
      },
      {
          Header:'In Patient Expenditure',
          accessor:'inpatientExpenditure'
      },
      {
          Header:'OPD Utilization Rate',
          accessor:'opdUtilizationRate'
      }
  
  ];
  
  export const MarginalizedColumns = [
    {
      Header: "County Code",
      accessor: "countyCode",
      Cell: props =>editCountyCode(props.value)
    },
    {
      Header: "Constituency",
      accessor: "constituency",
    },
    {
      Header: "Ward",
      accessor: "ward",
    },
    {
      Header: "Marginalized Area",
      accessor: "name",
    },
    {
      Header: "Population",
      accessor: "population",
      Cell: props => numberWithCommas(props.value) 
    },
    {
      Header: "Allocation Index",
      accessor: "allocationIndex",
    },
  ];

  export const MarginalizedFirstPolicyColumns = [
    {
      Header: "County",
      accessor: "county",
      style: {
        fontWeight: 'bold'
    },
    },
    {
      Header: "Population",
      accessor: "countyPopulation",
    },
    {
      Header: "Percentage Share of CDI",
      accessor: "percentageShareOfCDI",
    },
    {
      Header: "Percentage of Equal Share",
      accessor: "percentageOfEqualShare",
    },
    {
      Header: "Percentage of Combined share",
      accessor: "percentageCombinedShare",
    },
    {
      Header: "Funding Allocated",
      accessor: "funding",
      Cell: props => numberWithCommas(props.value) 
    },
  ];

  export const CaraColumns = [
    {
      Header: "County",
      accessor: "countyName",
    },
    {
      Header: "CARA in KSH",
      accessor: "ceiling",
      Cell: props => numberWithCommas(props.value) 
    },
    
  ];

  export const DisbursementColumns = [
    {
      Header: "Financial year",
      accessor: "year",
    },
    {
      Header: "County Government",
      accessor: "countyGovernment",
      Cell: props => numberWithCommas(props.value) 
    },
    {
      Header: "National Government",
      accessor: "nationalGovernment",
      Cell: props => numberWithCommas(props.value) 
    },
   
  ];

  export const AllocationsColumns = [
    
    {
      Header: "County Code",
      accessor: "countyCode",
      Cell: props =>editCountyCode(props.value)
    },
    {
      Header: "County",
      accessor: "countyName",
    },
    {
      Header: "Equitable Share",
      accessor: "equitableShare",
      Cell: props => numberWithCommas(props.value) 
    },
    {
      Header: "National Government Conditional grants",
      accessor: "totalNationalGrants",
      Cell: props => numberWithCommas(props.value) 
    },
    
    {
      Header: "Loans and Grants from Donors",
      accessor: "totalLoansGrants",
      Cell: props => numberWithCommas(props.value) 
    },
    {
      Header: "Total transfers to counties",
      accessor: "totalRevenue",
      Cell: props => numberWithCommas(props.value) 
    },
   
  ];
  export const AllocationColumns = [
    
   
    {
      Header: "Year",
      accessor: "year",
    },
    {
      Header: "Equitable Share",
      accessor: "equitableShare",
      Cell: props => numberWithCommas(props.value) 
    },
    {
      Header: "National Government Conditional grants",
      accessor: "totalNationalGrants",
      Cell: props => numberWithCommas(props.value) 
    },
    {
      Header: "Total National Transfers",
      accessor: "totalNationalTransfers",
      Cell: props => numberWithCommas(props.value) 
    },
    {
      Header: "Loans and Grants from Donors",
      accessor: "totalLoansGrants",
      Cell: props => numberWithCommas(props.value) 
    },
    {
      Header: "Total Revenue to County",
      accessor: "totalRevenue",
      Cell: props => numberWithCommas(props.value) 
    },
   
  ];
  
  export const CoditionalGrantsColumns = [
    
    {
      Header: "County Code",
      accessor: "countyCode",
      Cell: props => editCountyCode(props.value) 
    },
    {
      Header: "County",
      accessor: "countyName",
    },
    
    {
      Header: "National Government Conditional grants",
      accessor: "totalNationalGrants",
      Cell: props => numberWithCommas(props.value) 
    },
    
    {
      Header: "Loans and Grants from Donors",
      accessor: "totalLoansGrants",
      Cell: props => numberWithCommas(props.value) 
    },
    
   
  ];

  export const OSRColumns = [
    {
      Header: "County",
      accessor: "countyName",
    },
    {
      Header: "2013 Q4",
      accessor: "year_thirteen",
      Cell: props => numberWithCommas(props.value) 

    },
    {
      Header: "2013/2014",
      accessor: "year_fourteen",
      Cell: props => numberWithCommas(props.value) 

    },
    {
      Header: "2014/2015",
      accessor: "year_fifteen",
      Cell: props => numberWithCommas(props.value) 

    },
    {
      Header: "2015/2016",
      accessor: "year_sixteen",
      Cell: props => numberWithCommas(props.value) 

    },
    {
      Header: "2016/2017",
      accessor: "year_seventeen",
      Cell: props => numberWithCommas(props.value) 

    },
    {
      Header: "2017/2018",
      accessor: "year_eighteen",
      Cell: props => numberWithCommas(props.value) 

    },
    {
      Header: "2018/2019",
      accessor: "year_nineteen",
      Cell: props => numberWithCommas(props.value) 

    },
    {
      Header: "2019/2020",
      accessor: "year_twenty",
      Cell: props => numberWithCommas(props.value) 

    },
    {
      Header: "2020/2021",
      accessor: "year_twentyOne",
      Cell: props => numberWithCommas(props.value) 

    },
  ];

  export const CeilingsColumns = [
    {
      Header: "County",
      accessor: "countyName",
    },
   
    {
      Header: "2013/2014",
      accessor: "year_fourteen",
      Cell: props => numberWithCommas(props.value) 

    },
    {
      Header: "2014/2015",
      accessor: "year_fifteen",
      Cell: props => numberWithCommas(props.value) 

    },
    {
      Header: "2015/2016",
      accessor: "year_sixteen",
      Cell: props => numberWithCommas(props.value) 

    },
    {
      Header: "2016/2017",
      accessor: "year_seventeen",
      Cell: props => numberWithCommas(props.value) 

    },
    {
      Header: "2017/2018",
      accessor: "year_eighteen",
      Cell: props => numberWithCommas(props.value) 

    },
    {
      Header: "2018/2019",
      accessor: "year_nineteen",
      Cell: props => numberWithCommas(props.value) 

    },
    {
      Header: "2019/2020",
      accessor: "year_twenty",
      Cell: props => numberWithCommas(props.value) 

    },
    {
      Header: "2020/2021",
      accessor: "year_twentyOne",
      Cell: props => numberWithCommas(props.value) 

    },
  ];

  export const PopulationColumns = [
    {
      Header: "County",
      accessor: "countyName",
    },
    {
        Header: "Total Population",
        accessor: "totalPopulation",
        Cell: props => numberWithCommas(props.value) 
    },
    {
      Header: "Rural Population",
      accessor: "rural",
      Cell: props => numberWithCommas(props.value) 
    },
    {
      Header: "Urban Population",
      accessor: "urban",
      Cell: props => numberWithCommas(props.value) 
    },
    {
      Header: "Female",
      accessor: "female",
      Cell: props => numberWithCommas(props.value) 

    },
    {
      Header: "Male",
      accessor: "male",
      Cell: props => numberWithCommas(props.value) 

    },
    {
      Header: "Intersex",
      accessor: "intersex",
      Cell: props => numberWithCommas(props.value) 

    },
    {
      Header: "Rural Households",
      accessor: "ruralHouseholds",
      Cell: props => numberWithCommas(props.value) 

    },
    {
      Header: "Urban Households",
      accessor: "urbanHouseholds",
      Cell: props => numberWithCommas(props.value) 

    },
    {
      Header: "Population Density",
      accessor: "populationDensity",
      Cell: props => numberWithCommas(props.value) 

    },
  ];

  export const HealthStaffCOLUMNS = [
    
    {
      Header: "County",
      accessor: "countyName",
    },
    {
      Header: "Clinical Officers",
      accessor: "clinicalOfficers",
    },
    {
      Header: "community Health Service Staff",
      accessor: "communityHealthServiceStaff",
    },
    {
      Header: "dental Staff",
      accessor: "dentalStaff",
    },
    {
      Header: "diagnostics And Imaging",
      accessor: "diagnosticsAndImaging",
    },
    {
      Header: "environmental Health Staff",
      accessor: "environmentalHealthStaff",
    },
    {
      Header: "health Administrative Staff",
      accessor: "healthAdministrativeStaff",
    },
    
    {
      Header: "health Information On ICT",
      accessor: "healthInformationOnICT",
    },
    {
      Header: "hospital Maintenance Staff",
      accessor: "hospitalMaintenanceStaff",
    },
    {
      Header: "medical Laboratory Scientists",
      accessor: "medicalLaboratoryScientists",
    },
    {
      Header: "medical Officers And Specialists",
      accessor: "medicalOfficersAndSpecialists",
    },
    {
      Header: "medical Social Workers",
      accessor: "medicalSocialWorkers",
    },
    {
      Header: "nurses And Specialist Nurses",
      accessor: "nursesAndSpecialistNurses",
    },
    {
      Header: "nutrition Staff",
      accessor: "nutritionStaff",
    },
    {
      Header: "pharmacy Staff",
      accessor: "pharmacyStaff",
    },
    {
      Header: "Plaster Staff",
      accessor: "plasterStaff",
    },
    {
      Header: "Rehabilitative Staff",
      accessor: "rehabilitativeStaff",
    },
    {
      Header: "Support Staff",
      accessor: "supportStaff",
    },
    {
      Header: "Health Promotion Officers",
      accessor: "healthPromotionOfficers",
    },
    {
      Header: "HTS Counselor",
      accessor: "htscounselor",
    }
  ];

  export const EquitableColumns = [
    {
      Header: "County",
      accessor: "countyName",
    },
    {
      Header: "Equitable Share",
      accessor: "equitableShare",
      Cell: props => numberWithCommas(props.value) 
    },
    {
      Header: "Year",
      accessor: "year",
      Cell: props => numberWithCommas(props.value) 
    },
  ];

  export const LoansColumns = [
    {
      Header: "County",
      accessor: "countyName",
    },
    {
      Header: "Total Loans and Grants",
      accessor: "totalLoansGrants",
      Cell: props => numberWithCommas(props.value) 
    },
    {
      Header: "Year",
      accessor: "year",
    },
  ];

  export const MarginalizedThirdColumns = [
    {
      Header: "County",
      accessor: "countyName",
    },
    {
      Header: "Number of Constuencies",
      accessor: "numberOfConstituencies",
      Cell: props => numberWithCommas(props.value) 
    },
    {
      Header: "Number of Sublocations",
      accessor: "numberOfSubLocations",
      Cell: props => numberWithCommas(props.value) 
    },
    {
      Header: "Total Funding",
      accessor: "totalFunding",
      Cell: props => numberWithCommas(props.value) 
    },
  ];
  
  export const MarginalizationAddendumColumns = [
    {
      Header: "County",
      accessor: "countyName",
    },
    {
      Header: "Criteria for sharing revenue",
      accessor: "criteria",
    },
    {
      Header: "Total Allocation",
      accessor: "totalAllocation",
      Cell: props => numberWithCommas(props.value) 
    },
    
  ];

  function numberWithCommas(x) {
        return x.toLocaleString("en-US");
      }
  function editCountyCode(x){
      return x<10? "00"+x : "0"+x;
  }
  