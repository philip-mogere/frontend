import React, { useEffect, useMemo, useState } from "react";
import { useTable, useGlobalFilter } from "react-table";
import { MarginalizedFirstPolicyColumns } from "./Columns";
import GlobalFilter from "./GlobalFilter";

export default function MarginalizedFirstPolicy() {
  const [marginalizedCounty, setMarginalizedCounty] = useState([]);

  useEffect(() => {
    fetchMarginalizedAreas();
  }, []);

  const fetchMarginalizedAreas = async () => {
    const data = await fetch(
      `/county/marginalizedCounty/`
    );
    const dataJson = await data.json();
    setMarginalizedCounty(dataJson);
  };
  const columns = useMemo(() => MarginalizedFirstPolicyColumns, []);
  const data = useMemo(() => marginalizedCounty);

  const tableInstance = useTable(
    {
      columns,
      data,
    },
    useGlobalFilter
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    state,
    setGlobalFilter,
  } = tableInstance;

  const { globalFilter } = state;

  return (
    <div>
      <span
        style={{ cursor: "pointer" }}
        onClick={() => (window.location.href = "/marginalized")}
      >
        Back to 2nd policy
      </span>
      <h2 className="text-center mt-5">Marginalization First Policy</h2>
      <p>
        The First Policy The First Policy was for a period of three years:
        2014/15; 2015/16 and 2016/17, and appropriated a total sum of Ksh 12.4
        billion among 14 counties.
      </p>
      <GlobalFilter filter={globalFilter} setFilter={setGlobalFilter} />
      <table {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps()}>{column.render("Header")}</th>
              ))}
            </tr>
          ))}
        </thead>

        <tbody {...getTableBodyProps()}>
          {rows.map((row) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map((cell) => {
                  return (
                    <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
