import React from "react";
import { Form } from "react-bootstrap";

export default function GlobalFilter({ filter, setFilter }) {
  return (
    <>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Control
          placeholder="Search table"
          value={filter || ""}
          onChange={(e) => setFilter(e.target.value)}
        />
      </Form.Group>
    </>
  );
}
