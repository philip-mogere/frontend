import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { Potraits } from "./Potraits";
import { Potraitsdg } from "./Potraitsdg";
import { Link } from "react-router-dom";

import "./politicalDetails.css";
import NavBar from "../NavBar/NavBar";

export default function PoliticalDetails(county) {
  const [executive, setExecutive] = useState({
    cecs: {},
  });
  const cecs = Array.from(executive.cecs);
  const code = county.match.params.countyCode;
console.log(county);
  useEffect(() => {
    fetchExecutive();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fetchExecutive = async () => {
    const data = await fetch(`/county/${code}/executive`);
    const dataJson = await data.json();
    setExecutive(dataJson);
  };

  return (
    <>
      <NavBar county={county.match} className="navBar" />
      <div className="executiveInfo">
        <h3>County Executive </h3>
        <hr />
        <div className="governor text-center">
          <img className="coa" alt="County Governor" src={Potraits[code - 1]} />
          <Link to={`profile`}>
            <h3>Governor</h3>
          </Link>
          <h3>H.E. {executive.governor}</h3>
          <hr />
          {Potraitsdg[code - 1] ? (
            <img
              className="coa mb-3"
              alt="County Deputy Governor"
              src={Potraitsdg[code - 1]}
            />
          ) : (
            <img
              className="coa mb-3"
              alt="County Deputy Governor"
              src={Potraits[Potraits.length - 1]}
            />
          )}

          <h3 className="detailsInfo">Deputy Governor</h3>
          <h3>Hon. {executive.deputyGovernor}</h3>
        </div>
        <hr />
        <div className="otherDetails ">
          <div className="text-center">
            
            <h3 className="detailsInfo">
              County Secretary: {executive.countySecretary}
            </h3>
          </div>
          <hr />
          <table>
            <thead>
              <tr>
                <th>County Executive Committee Members</th>
                <th>Docket</th>
              </tr>
            </thead>
            <tbody>
              {cecs.map((cec) => (
                <tr key={cec.cecid}>
                  <td>Hon. {cec.cecname} </td>
                  <td>{cec.docket}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
}
