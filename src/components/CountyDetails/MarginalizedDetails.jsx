import React, { useEffect, useState } from "react";
import GoogleMap from "../GoogleMap/GoogleMap";
import "./MarginalizedDetails.css";
import NavBar from "../NavBar/NavBar";

export default function MarginalizedDetails(county) {
  const [marginalized, setMarginalized] = useState({});
  const code = county.match.params.countyCode;
  const marginalizedAreas = Array.from(marginalized);

  useEffect(() => {
    fetchCounty();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fetchCounty = async () => {
    const data = await fetch(
      `/county/${code}/marginalized`
    );
    const dataJson = await data.json();
    setMarginalized(dataJson);
  };
  return (
    <div className="marginalized">
      <NavBar county={county.match} className="navBar" />
      <div className="detailsBox ">
        <div className="detailsInfoBox">
          {/* <h2 className='detailsInfo'>{sentCounty.countyName} county</h2> */}
          <h3 className="detailsInfo">
            Marginalized Areas in {county.match.params.countyName} County
          </h3>
          <p className="detailsInfo">Number of Marginalized Areas:</p>
          <h2 className="detailsInfo">{marginalizedAreas.length}</h2>
          <ol className="detailsInfo">
            {marginalizedAreas.map((marginalizedArea) => (
              <li key={marginalizedArea.id}>{marginalizedArea.name}</li>
            ))}
          </ol>
        </div>
        <div className="googleMap">
          <GoogleMap />
        </div>
      </div>
    </div>
  );
}
