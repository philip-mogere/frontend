import React, { useState, useEffect } from "react";
import Table from "../Tables/Table";
import { HealthStaffCOLUMNS } from "../Tables/Columns";

export default function HealthStaff() {
  const [healthStaff, setHealthStaff] = useState([]);

  useEffect(() => {
    fetchCounties();
  }, []);
  const fetchCounties = async () => {
    const data = await fetch("/county/healthStaff");
    const dataJson = await data.json();
    setHealthStaff(dataJson);
  };

  return (
    <div>
      <span
        style={{ cursor: "pointer", color: "white" }}
        onClick={() => (window.location.href = "/")}
      >
        Back to home
      </span>
      <Table Columns={HealthStaffCOLUMNS} Data={healthStaff} />
    </div>
  );
}
