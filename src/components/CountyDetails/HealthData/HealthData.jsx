import React, { useState, useEffect } from "react";
import HealthDataTable from "../Tables/HealthDataTable";


export default function HealthData() {
  const [healthData, setHealthData] = useState([]);

  useEffect(() => {
    fetchCounties();
  }, []);
  const fetchCounties = async () => {
    const data = await fetch(`/county/healthData`);
    const dataJson = await data.json();
    setHealthData(dataJson);
  };

  return (
    <div>
      <span
        style={{ cursor: "pointer", color: "white" , marginBottom:'100px'}}
        onClick={() => (window.location.href = "/")}
      >
        Back to home
      </span>
      <HealthDataTable healthData={healthData} />
    </div>
  );
}
