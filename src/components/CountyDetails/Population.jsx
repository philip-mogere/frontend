import React, { useEffect, useState } from "react";
// import Table from "./Tables/Table";
import { HealthStaffCOLUMNS } from "./Tables/Columns";
import NavBar from "../NavBar/NavBar";

export default function Population(county) {
  const [sentCounty, setCounty] = useState({
    populationInfo: {},
  });
  const [healthStaff, setHealthStaff] = useState({});

  console.log(healthStaff);
  const code = county.match.params.countyCode;

  const populations = Array.from(sentCounty.populationInfo);

  useEffect(() => {
    fetchCounty();
    fetchHealthStaff();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fetchCounty = async () => {
    const data = await fetch(`/county/extended/${code}`);
    const dataJson = await data.json();
    setCounty(dataJson);
  };

  const fetchHealthStaff = async () => {
    const data = await fetch(`/county/${code}/healthStaff`);
    const dataJson = await data.json();
    setHealthStaff(dataJson);
  };

  function numberWithCommas(x) {
    return x.toLocaleString("en-US");
  }

  return (
    <div>
      <NavBar county={county.match} className="navBar" />
      <div >
        <br />
<br />
        <h3 className="detailsInfo">County Population </h3>
        <table className="detailsInfo">
          <th>Year</th>
          <th>Male</th>
          <th>female</th>
          <th>Intersex</th>
          <th>Rural Population</th>
          <th>Urban Population</th>
          <th>Population Density (per Sq. km)</th>
          <th>Total Population</th>

          {populations.map((population) => (
            <tr>
              <td>{population.year}</td>
              <td>{numberWithCommas(population.male)}</td>
              <td>{numberWithCommas(population.female)}</td>
              <td>{numberWithCommas(population.intersex)}</td>
              <td>{numberWithCommas(population.rural)}</td>
              <td>{numberWithCommas(population.urban)}</td>
              <td>{population.populationDensity}</td>
              <td>{numberWithCommas(population.totalPopulation)}</td>
            </tr>
          ))}
        </table>
<br />
            <h3>Health Staff information</h3>
        <table className="detailsInfo">
          <th>Clinical Officers</th>
          <th>Community Health Service Staff</th>
          <th>dentalStaff</th>
          <th>diagnosticsAndImaging</th>
          <th>environmentalHealthStaff</th>
          <th>healthAdministrativeStaff</th>
          <th>healthInformationOnICT </th>
          <th>hospitalMaintenanceStaff </th>
          {/* <th>medicalLaboratoryScientists</th>
          <th>medicalOfficersAndSpecialists</th>
          <th>medicalSocialWorkers</th>
          <th>nursesAndSpecialistNurses</th>
          <th>nutritionStaff</th>
          <th>pharmacyStaff</th>
          <th>plasterStaff</th>
          <th>rehabilitativeStaff</th>
          <th>supportStaff</th>
          <th>healthPromotionOfficers</th>
          <th>htscounselor</th> */}


            <tr>
              <td>{healthStaff.clinicalOfficers}</td>
              <td>{healthStaff.communityHealthServiceStaff}</td>
              <td>{healthStaff.dentalStaff}</td>
              <td>{healthStaff.diagnosticsAndImaging}</td>
              <td>{healthStaff.environmentalHealthStaff}</td>
              <td>{healthStaff.healthAdministrativeStaff}</td>
              <td>{healthStaff.healthInformationOnICT}</td>
              <td>{healthStaff.hospitalMaintenanceStaff}</td>
              {/* <td>{healthStaff.medicalLaboratoryScientists}</td>
              <td>{healthStaff.medicalOfficersAndSpecialists}</td>
              <td>{healthStaff.medicalSocialWorkers}</td>
              <td>{healthStaff.nursesAndSpecialistNurses}</td>
              <td>{healthStaff.nutritionStaff}</td>
              <td>{healthStaff.pharmacyStaff}</td>
              <td>{healthStaff.plasterStaff}</td>
              <td>{healthStaff.rehabilitativeStaff}</td>
              <td>{healthStaff.supportStaff}</td>
              <td>{healthStaff.healthPromotionOfficers}</td>
              <td>{healthStaff.htscounselor}</td> */}
            </tr>
          
        </table>
      </div>
    </div>
  );
}
