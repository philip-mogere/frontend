import Garissa from './PotraitsDSpeaker/Garissa-dspeaker.png'
import Kwale from './PotraitsDSpeaker/KwaleDspeaker.jpeg'
import Nyeri from './PotraitsDSpeaker/NyeriDSpeaker.jpeg'
import Kirinyaga from './PotraitsDSpeaker/KirinyagaDSpeaker.jpeg'
import Nyamira from './PotraitsDSpeaker/NyamiraDSpeaker.jpg'

import Placeholder from './PotraitsSpeaker/Placeholder.jpeg'


export const PotraitsDSpeaker=[
    Kwale,
    Garissa,
    Nyeri,
    Kirinyaga,
    Nyamira,
    Placeholder
    
]