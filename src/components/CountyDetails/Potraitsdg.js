import Mombasa from './Potraitsdg/Mombasa.png'
import Kwale from './Potraitsdg/Kwale.png'
import Kilifi from './Potraitsdg/Kilifi-DGov.jpeg'
import Tana from './Potraitsdg/Tana-DGov.jpeg'
import Lamu from './Potraitsdg/Placeholder.jpeg'
import Taita from './Potraitsdg/TaitaDG.jpeg'
import Garissa from './Potraitsdg/Garissa-dg.png'
import Wajir from './Potraitsdg/Wajir-dg.png'
import Mandera from './Potraitsdg/ManderaDG.jpeg'
import Marsabit from './Potraitsdg/MaesabitDG.jpeg'
import Isiolo from './Potraitsdg/IsioloDG.jpeg'
import Meru from './Potraitsdg/MeruDG.jpeg'
import Tharaka from './Potraitsdg/TharakaDG.jpeg'
import Embu from './Potraitsdg/EmbuDG.jpeg'
import Kitui from './Potraitsdg/KituiDG.jpeg'
import Machakos from './Potraitsdg/MachakosDG.jpeg'
import Makueni from './Potraitsdg/MakueniDG.jpeg'
import Nyandarua from './Potraitsdg/NyandaruaDG.jpeg'
import Nyeri from './Potraitsdg/NyeriDG.jpeg'
import Kirinyaga from './Potraitsdg/KirinyagaDG.jpeg'
import Muranga from './Potraitsdg/MurangaDG.jpeg'
import Kiambu from './Potraitsdg/KiambuDG.jpeg'
import Turkana from './Potraitsdg/TurkanaDG.jpeg'
import Pokot from './Potraitsdg/PokotDG.jpeg'
import Nzoia from './Potraitsdg/NzoiaDG.jpeg'
import Samburu from './Potraitsdg/SamburuDG.jpg'
import Uasin from './Potraitsdg/UasinDG.jpeg'
import Marakwet from './Potraitsdg/MarakwetDG.jpeg'
import Nandi from './Potraitsdg/NandiDG.jpeg'
import Baringo from './Potraitsdg/BaringoDG.jpeg'
import Laikipia from './Potraitsdg/LaikipiaDG.jpeg'
import Nakuru from './Potraitsdg/NakuruDG.jpeg'
import Narok from './Potraitsdg/NarokDG.jpeg'
import Kajiado from './Potraitsdg/KajiadoDG.jpeg'
import Kericho from './Potraitsdg/KerichoDG.jpeg'
import Bomet from './Potraitsdg/BometDG.jpeg'
import Kakamega from './Potraitsdg/KakamegaDG.jpeg'
import Vihiga from './Potraitsdg/VihigaDG.jpeg'
import Bungoma from './Potraitsdg/BungomaDG.jpeg'
import Busia from './Potraitsdg/BusiaDG.jpeg'
import Siaya from './Potraitsdg/SiayaDG.jpeg'
import Kisumu from './Potraitsdg/Kisumu.jpeg'
import Homabay from './Potraitsdg/HomabayDG.jpg'
import Migori from './Potraitsdg/MigoriDG.jpg'
import Kisii from './Potraitsdg/KisiiDG.jpg'
import Nyamira from './Potraitsdg/NyamiraDG.jpeg'
import Nairobi from './Potraitsdg/NairobiDG.jpeg'
import Placeholder from './Potraitsdg/Placeholder.jpeg'



export const Potraitsdg=[
    Mombasa,
    Kwale,
    Kilifi,
    Tana,
    Lamu,
    Taita,
    Garissa,
    Wajir,
    Mandera,
    Marsabit,
    Isiolo,
    Meru,
    Tharaka,
    Embu,
    Kitui,
    Machakos,
    Makueni,
    Nyandarua,
    Nyeri,
    Kirinyaga,
    Muranga,
    Kiambu,
    Turkana,
    Pokot,
    Samburu,
    Nzoia,
    Uasin,
    Marakwet,
    Nandi,
    Baringo,
    Laikipia,
    Nakuru,
    Narok,
    Kajiado,
    Kericho,
    Bomet,
    Kakamega,
    Vihiga,
    Bungoma,
    Busia,
    Siaya,
    Kisumu,
    Homabay,
    Migori,
    Kisii,
    Nyamira,
    Nairobi,
    Placeholder,
]