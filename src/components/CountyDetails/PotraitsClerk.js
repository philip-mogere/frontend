import Mombasa from './PotraitsClerk/Mombasa-Clerk.jpg'
import Kwale from './PotraitsClerk/Kwale-Clerk.jpeg'
import Kilifi  from './PotraitsClerk/Kilifi-Clerk.jpeg'
import Nyeri from './PotraitsClerk/NyeriClerk.jpeg'
import Muranga from './PotraitsClerk/MurangaClerk.jpeg'
import Kirinyaga from './PotraitsClerk/KirinyagaClerk.jpeg'
import Kisii from './PotraitsClerk/KisiiClerk.jpg'
import Placeholder from './PotraitsSpeaker/Placeholder.jpeg'





export const PotraitsClerk=[
    Mombasa,
    Kwale,
    Kilifi,
    Placeholder,
    Nyeri,
    Kirinyaga,
    Muranga,
    Kisii,
    Placeholder   
]