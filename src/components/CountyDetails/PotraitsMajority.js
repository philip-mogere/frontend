import Mombasa from './PotraitsMajority/Mombasa-Majority.jpeg'
import Kwale from './PotraitsMajority/Kwale-Majority.jpeg'
import Kilifi  from './PotraitsMajority/Kilifi-Majority.jpeg'
import Tana from './PotraitsMajority/TanaMajority.jpeg'
import Lamu from './PotraitsSpeaker/Placeholder.jpeg'
import Taita from './PotraitsSpeaker/Placeholder.jpeg'
import Garissa from './PotraitsSpeaker/Placeholder.jpeg'
import Wajir from './PotraitsMajority/Wajir-Majority.png'
import Tharaka from './PotraitsMajority/TharakaMajority.jpeg'
import Embu from './PotraitsMajority/EmbuMajority.jpeg'
import Kitui from './PotraitsMajority/KituiMajority.jpeg'
import Nyeri from './PotraitsMajority/NyeriMajority.jpeg'
import Kirinyaga from './PotraitsMajority/KirinyagaMajority.jpeg'
import Machakos from './PotraitsMajority/MachakosMajority.jpeg'
import Makueni from './PotraitsMajority/MakueniMajority.jpeg'
import Nyandarua from './PotraitsMajority/NyandaruaMajority.jpeg'
import Placeholder from './PotraitsSpeaker/Placeholder.jpeg'





export const PotraitsMajority=[
    Mombasa,
    Kwale,
    Kilifi,
    Tana,
    Lamu,
    Taita,
    Garissa,
    Wajir,
    Placeholder,
    Placeholder,
    Placeholder,
    Placeholder,
    Tharaka,
    Embu,
    Kitui,
    Machakos,
    Makueni,
    Nyandarua,
    Nyeri,
    Kirinyaga,
    Placeholder
    
]