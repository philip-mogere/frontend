import React, { useEffect, useState } from "react";
import "./Subcounties.css";
import NavBar from "../NavBar/NavBar";

export default function Subcounties(county) {
  const [subCountyList, setSubCountyList] = useState({});
  const code = county.match.params.countyCode;

  const subCounties = Array.from(subCountyList);

  useEffect(() => {
    fetchCounty();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fetchCounty = async () => {
    const data = await fetch(`/county/${code}/subcounty`);
    const dataJson = await data.json();
    console.log(dataJson);
    setSubCountyList(dataJson);
  };
  return (
    <div className="subcounties">
      <NavBar county={county.match} className="navBar" />
      <div className="detailsBox">
        <div className="subcountiesInfo">
          <h3 className="detailsInfo">Sub Counties</h3>
          <p className="detailsInfo">Number of SubCounties:</p>
          <h2 className="subcountiesCount detailsInfo">{subCounties.length}</h2>
          <ol>
            {subCounties.map((subCounty) => (
              <li key="subCounties.id" className="detailsInfo">
                {subCounty.name}
              </li>
            ))}
          </ol>
        </div>
        <div className="subcountiesMap"></div>
      </div>
    </div>
  );
}
