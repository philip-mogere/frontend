import React from 'react'
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import './countyBudget.css'



export default function CountyBudgetChart(budgets) {

    const data = budgets.budgets   
    
    return (
        <div className="chart">
            <h3>County Budgets in Millions (Ksh)</h3>

            <ResponsiveContainer width="110%" aspect={4/2.5}>
                <LineChart data={data}>
                    <XAxis dataKey="year"/>
                    <YAxis dataKey="development"/>
                    <Line type="monotone" dataKey="recurrent" stroke="#f00000"/>
                    <Line type="monotone" dataKey="development" stroke="#60c018"/>
                    <Tooltip/> 
                    <Legend/>
                    <CartesianGrid/>
                </LineChart>
            </ResponsiveContainer>
        </div>
    )
}
