import React, { Fragment } from "react";
import { useRouteMatch, Route, Switch } from "react-router-dom";
import NavBar from "../NavBar/NavBar";
import Sidebar from "../SideBar/Sidebar";
import CountyAssembly from "./CountyAssembly";
import CountyBudget from "./CountyBudget";
import { CountyDetail } from "./CountyDetail";
import MarginalizedDetails from "./MarginalizedDetails";
import PoliticalDetails from "./PoliticalDetails";
import Population from "./Population";

export const CountyHome = () => {
  let match =useRouteMatch()
  return (
    <Fragment>
      <NavBar county={match} className="navBar" />
      <div className="d-flex">
        <Sidebar />
        <div>
          <Switch>

            <Route path={`${match.url}`}>
              <CountyDetail match={match} />
            </Route>

            <Route path={`${match.url}/executive`}>
              <PoliticalDetails match={match}/>
            </Route>
           
            <Route path={`${match.url}/assembly`}>
              <CountyAssembly />
            </Route>
            <Route
              exact
              path={`${match.url}/marginalized`}
              component={MarginalizedDetails}
            />
            <Route
              exact
              path={`${match.url}/population`}
              component={Population}
            />
            <Route
              exact
              path={`${match.url}/budget`}
              component={CountyBudget}
            />
          </Switch>
        </div>
      </div>
    </Fragment>
  );
};
