import Mombasa from './Potraits/Mombasa.jpeg'
import Kwale from './Potraits/Kwale.jpeg'
import Kilifi from './Potraits/Kilifi.jpeg'
import Tana  from './Potraits/Tana.jpeg'
import Lamu  from './Potraits/Lamu.jpeg'
import Taveta  from './Potraits/Taita.jpeg'
import Garissa from './Potraits/Garissa.jpeg'
import Wajir  from './Potraits/Wajir.png'
import Mandera from './Potraits/Mandera.jpeg'
import Marsabit  from './Potraits/Marsabit.jpeg'
import Isiolo  from './Potraits/Isiolo.jpeg'
import Meru  from './Potraits/Meru.jpeg'
import Tharaka  from './Potraits/Tharaka.jpeg'
import Embu  from './Potraits/Embu.jpeg'
import Kitui  from './Potraits/Kitui.jpeg'
import Machakos  from './Potraits/Machakos.jpeg'
import Makueni  from './Potraits/Makueni.jpeg'
import Nyandarua  from './Potraits/Nyandarua.jpeg'
import Nyeri  from './Potraits/Nyeri.jpeg'
import Kirinyaga  from './Potraits/Kirinyaga.jpeg'
import Muranga  from './Potraits/Muranga.jpeg'
import Kiambu  from './Potraits/Kiambu.jpeg'
import Turkana  from './Potraits/Turakana.jpeg'
import Pokot  from './Potraits/Pokot.jpeg'
import Samburu  from './Potraits/Samburu.jpeg'
import Trans  from './Potraits/Nzoia.jpeg'
import Uasin  from './Potraits/Uasin.jpeg'
import Elgeyo  from './Potraits/Marakwet.jpeg'
import Nandi  from './Potraits/Nandi.jpeg'
import Baringo from './Potraits/Baringo.jpeg'
import Laikipia from './Potraits/Laikipia.jpeg'
import Nakuru from './Potraits/Nakuru.jpeg'
import Narok from './Potraits/Narok.jpeg'
import Kajiado from './Potraits/Kajiado.jpeg'
import Kericho from './Potraits/Kericho.jpeg'
import Bomet from './Potraits/Bomet.jpeg'
import Kakamega from './Potraits/Kakamega.jpeg'
import Vihiga from './Potraits/Vihiga.jpeg'
import Bungoma from './Potraits/Bungoma.jpeg'
import Busia from './Potraits/Busia.jpeg'
import Siaya from './Potraits/Siaya.jpeg'
import Kisumu from './Potraits/Kisumu.jpeg'
import Homabay from './Potraits/Homabay.jpeg'
import Migori from './Potraits/Migori.jpeg'
import Kisii from './Potraits/Kisii.jpeg'
import Nyamira from './Potraits/Nyamira.jpeg'
import Nairobi from './Potraits/Nairobi.jpeg'
import Placeholder from './Potraits/Placeholder.jpeg'




export const Potraits=[
    Mombasa,
    Kwale,
    Kilifi, 
    Tana,
    Lamu,
    Taveta,
    Garissa,
    Wajir, 
    Mandera,
    Marsabit,
    Isiolo,
    Meru,
    Tharaka,
    Embu,
    Kitui,
    Machakos,
    Makueni,
    Nyandarua,
    Nyeri,
    Kirinyaga,
    Muranga,
    Kiambu,
    Turkana,
    Pokot,
    Samburu,
    Trans,
    Uasin,
    Elgeyo,
    Nandi,
    Baringo,
    Laikipia,
    Nakuru,
    Narok,
    Kajiado,
    Kericho,
    Bomet,
    Kakamega,
    Vihiga,
    Bungoma,
    Busia,
    Siaya,
    Kisumu,
    Homabay,
    Migori,
    Kisii,
    Nyamira,
    Nairobi,
    Placeholder
]