import Mombasa from './PotraitsSpeaker/Mombasa.jpeg'
import Kwale from './PotraitsSpeaker/Kwale.png'
import Tana  from './PotraitsSpeaker/Tana.jpeg'
import Kilifi from './PotraitsSpeaker/Kilifi-Speaker.jpeg'
import Garissa from './PotraitsSpeaker/Garissa-speaker.png'
import Wajir from './PotraitsSpeaker/Wajir-Speaker.png'
import Nyandarua from './PotraitsSpeaker/NyandaruaSpeaker.jpeg'
import Nyeri from './PotraitsSpeaker/NyeriSpeaker.jpeg'
import Kisii from './PotraitsSpeaker/KisiiSpeaker.jpg'
import Kitui from './PotraitsSpeaker/KituiSpeaker.jpeg'
import Kisumu from './PotraitsSpeaker/KisumuSpeaker.jpg'
import Tharaka from './PotraitsSpeaker/TharakaSpeaker.jpeg'
import Machakos from './PotraitsSpeaker/MachakosSpeaker.jpeg'
import Kirinyaga from './PotraitsSpeaker/KirinyagaSpeaker.jpeg'
import Kiambu from './PotraitsSpeaker/KiambuSpeaker.jpeg'
import Embu from './PotraitsSpeaker/EmbuSpeaker.jpeg'
import Makueni from './PotraitsSpeaker/MakueniSpeaker.jpeg'
import Migori from './PotraitsSpeaker/MigoriSpeaker.jpg'
import Nyamira from './PotraitsSpeaker/NyamiraSpeaker.jpg'
import Placeholder from './PotraitsSpeaker/Placeholder.jpeg'





export const PotraitsSpeaker=[
    Mombasa,
    Kwale,
    Kilifi,
    Tana,
    Garissa,
    Wajir,
    Placeholder,
    Placeholder,
    Placeholder,
    Placeholder,
    Tharaka,
    Embu,
    Kitui,
    Machakos,
    Makueni,
    Nyandarua,
    Nyeri,
    Kirinyaga,
    Placeholder,
    Kiambu,
    Placeholder,
    Placeholder,
    Placeholder,
    Placeholder,
    Placeholder,
    Placeholder,
    Placeholder,
    Placeholder,
    Placeholder,
    Placeholder,
    Placeholder,
    Placeholder,
    Placeholder,
    Placeholder,
    Placeholder,
    Placeholder,
    Placeholder,
    Placeholder,
    Placeholder,
    Kisumu,
    Placeholder,
    Migori,
    Kisii,
    Nyamira,
    Placeholder,
    Placeholder
    
]