
import React from 'react'
import { Container, Row, Col, Form, Button } from 'react-bootstrap'

export default function Login() {
    return (
        <Container className = "d-flex flex-column ">
            <Row className="justify-content-center align-items-center">
                <Col md="8" lg="8">
                    <Form.Group className="mb-2" controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" />
                    </Form.Group>
                </Col>
            </Row>
            <Row className="justify-content-center align-items-center">
                <Col md="8" lg="8">
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" />
                    </Form.Group>
                </Col>
            </Row> 
            <Row className="justify-content-center align-items-center">
                <Col md="8" lg="8" >
                    <Button variant="primary" type="submit">
                        Submit
                    </Button>

                    <Button 
                        variant="primary" 
                        type="submit"
                        onClick={()=>{
                            window.location.href = "/";
                        }}>
                        Exit
                    </Button>
                </Col>
           </Row>
            
        </Container>
    )
}
