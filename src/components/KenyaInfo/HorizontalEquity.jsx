import React, { useState } from "react";
import {Dropdown, Form } from "react-bootstrap";
import CountryAllocations from "./CountryAllocations";
import Equitable from "./Equitable";
import LoansAndGrants from "./LoansAndGrants";
import NationalGovernmentLoans from "./NationalGovernmentLoans";

export default function HorizontalEquity() {

    const [allocation, setAllocation] = useState("All");

  return (
    <div div className=" pt-5">
        <div>
          
        <Dropdown>
          <Dropdown.Toggle id="dropdown-basic">
            Select Allocation
          </Dropdown.Toggle>

          <Dropdown.Menu>
            <Dropdown.Item onClick={() => setAllocation("Equitable share")}>
              Equitable share
            </Dropdown.Item>
            {/* <Dropdown.Item
              onClick={() => setAllocation("National Government Loans")}
            >
              National Government conditional grants
            </Dropdown.Item> */}
            <Dropdown.Item onClick={() => setAllocation("Loans And Grants")}>
              Loans and Grants
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setAllocation("All")}>
              All
            </Dropdown.Item>
          </Dropdown.Menu>
          <Form className="pt-2">
          <Form.Text className="text-muted" style={{fontSize:'20px', paddingLeft:'1rem'}}>{allocation}</Form.Text>
        </Form>

        </Dropdown>
       
      </div>
      <div >
        {allocation === "Equitable share" ? (
          <Equitable />
        ) : allocation === "National Government Loans" ? (
          <NationalGovernmentLoans />
        ) : allocation === "Loans And Grants" ? (
          <LoansAndGrants />
        ) : (
          <CountryAllocations />
        )}
      </div>
    </div>
  )
}
