import React, { useEffect, useState } from "react";
import { Dropdown } from "react-bootstrap";
import { CaraColumns, CeilingsColumns } from "./../CountyDetails/Tables/Columns";
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

import Table from "../CountyDetails/Tables/Table";

export default function CountryCara() {
  const [cara, setCara] = useState([]);
  const [caras, setCaras] = useState([]);
  const [allocation, setAllocation] = useState("Assembly");
  const [assemblyCeiling, setAssemblyCeiling] = useState([]);
  const [executiveCeiling, setExecutiveCeiling] = useState([]);


  const [year, setYear] = useState(2021);

  useEffect(() => {
    fetchCaras();
    fetchCara();
    fetchExecutiveCeilings();
    fetchAssemblyCeilings();
  }, [year]);

  const fetchExecutiveCeilings = async () => {
    const data = await fetch(`/county/executive-ceiling`);
    const dataJson = await data.json();
    setExecutiveCeiling(dataJson);
  };

  const fetchAssemblyCeilings = async () => {
    const data = await fetch(`/county/assembly-ceiling`);
    const dataJson = await data.json();
    setAssemblyCeiling(dataJson);
  };

  const fetchCaras = async () => {
    const data = await fetch(`/county/caras?year=${year}`);
    const dataJson = await data.json();
    setCara(dataJson);
  };
  const fetchCara = async () => {
    const data = await fetch(`/county/cara`);
    const dataJson = await data.json();
    setCaras(dataJson);
  };

  return (
    <>
    
      <div className="cara">
        <h2 className="text-center">County Recurrent Expenditure Ceilings</h2>
        <p>
          The County Allocation Revenue Act is An Act of Parliament to provide
          for the Equitable Allocation of Revenue raised nationally among the
          county governments. Below is the breakdown for the monies allocated to
          each county for the year {year}.
        </p>

        <Dropdown>
          <Dropdown.Toggle id="dropdown-basic">
            Select Ceiling
          </Dropdown.Toggle>

          <Dropdown.Menu>
            <Dropdown.Item onClick={() => setAllocation("Assembly")}>
              County Assembly Ceilings
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setAllocation("Executive")}>
              County Executive Ceilings
            </Dropdown.Item>
          </Dropdown.Menu>
            <h3> County {allocation} Ceilings</h3>
        </Dropdown>

        <div >
        {allocation === "Assembly" ? (
          <Table Columns={CeilingsColumns} Data={assemblyCeiling} />
        ) : allocation === "National Government Loans" ? (
          <Table Columns={CeilingsColumns} Data={executiveCeiling} />
        ) : (
          <Table Columns={CeilingsColumns} Data={executiveCeiling} />
        )}
      </div>

        

        <ResponsiveContainer width="110%" aspect={4/2.5}>
                <LineChart data={caras}> 
                    <XAxis dataKey="year"/>
                    <YAxis dataKey="ceiling"/>
                    <Line type="monotone" dataKey="ceiling" stroke="#f00000"/>
                    <Tooltip/> 
                    <Legend/>
                    <CartesianGrid/>
                </LineChart>
            </ResponsiveContainer>
      </div>
    </>
  );
}
