import React from "react";
import { Tabs, Tab } from "react-bootstrap";
import HealthStaff from "../CountyDetails/HealthData/HealthStaff";
import HealthData from "../CountyDetails/HealthData/HealthData";
import CountryPopulation from "./CountryPopulation";

export default function Socio_economic() {
  return (
    <div >
      <Tabs
        fill
        defaultActiveKey="population"
        id="socio-economic"
      >
        <Tab eventKey="population" title="Population">
            <CountryPopulation/>
        </Tab>
        <Tab eventKey="vertical" title="Health Workers">
          <HealthStaff />
        </Tab>
        <Tab eventKey="horizontal" title="Health Data">
          <HealthData />
        </Tab>
        
      </Tabs>
    </div>
  );
}
