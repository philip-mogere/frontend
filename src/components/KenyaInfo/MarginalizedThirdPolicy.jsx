import React, { useEffect, useState } from "react";
import { MarginalizedThirdColumns } from "../CountyDetails/Tables/Columns";
import Table from '../CountyDetails/Tables/Table';

export default function MarginalizedThirdPolicy() {
    const [marginalizedCounty, setMarginalizedCounty] = useState([]);

  useEffect(() => {
    fetchMarginalizedAreas();
  }, []);

  const fetchMarginalizedAreas = async () => {
    const data = await fetch(
      `/county/marginalization-third/`
    );
    const dataJson = await data.json();
    setMarginalizedCounty(dataJson);
  };
  console.log(marginalizedCounty);
  return (
    <div>
        <Table Columns={MarginalizedThirdColumns} Data={marginalizedCounty}/>
    </div>
  )
}
