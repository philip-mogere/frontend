import React, { useEffect, useState } from "react";
import { Dropdown } from "react-bootstrap";
import { LoansColumns } from "./../CountyDetails/Tables/Columns";
import Table from "../CountyDetails/Tables/Table";

export default function LoansAndGrants() {
    const [loans, setLoans] = useState([]);
    const [year, setYear] = useState("2019/2020");
  
    useEffect(() => {
      fetchMarginalizedAreas();
    }, [year]);
  
    const fetchMarginalizedAreas = async () => {
      const data = await fetch(`/county/totalloans?year=${year}`);
      const dataJson = await data.json();
      setLoans(dataJson);
    };
console.log(loans);
    return (
        <>
      
        <div className="cara">
          <h2 className="text-center">Loans and Grants for the year {year}</h2>
         
          <Dropdown>
            <Dropdown.Toggle className="mb-3" id="dropdown-basic">
              Select Year
            </Dropdown.Toggle>
  
            <Dropdown.Menu>
              <Dropdown.Item onClick={() => setYear(2014)}>2014</Dropdown.Item>
              <Dropdown.Item onClick={() => setYear(2015)}>2015</Dropdown.Item>
              <Dropdown.Item onClick={() => setYear(2016)}>2016</Dropdown.Item>
              <Dropdown.Item onClick={() => setYear(2017)}>2017</Dropdown.Item>
              <Dropdown.Item onClick={() => setYear(2018)}>2018</Dropdown.Item>
              <Dropdown.Item onClick={() => setYear(2019)}>2019</Dropdown.Item>
              <Dropdown.Item onClick={() => setYear(2020)}>2020</Dropdown.Item>
              <Dropdown.Item onClick={() => setYear(2021)}>2021</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
          <Table Columns={LoansColumns} Data={loans} />
        </div>
      </>
    );
}
