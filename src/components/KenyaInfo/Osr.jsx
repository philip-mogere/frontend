import React, { useEffect, useState } from "react";
import { OSRColumns } from "./../CountyDetails/Tables/Columns";
import "./Osr.css";
import Table from "../CountyDetails/Tables/Table";

export default function Osr() {
  const [osr, setOsr] = useState([]);
  useEffect(() => {
    fetchMarginalizedAreas();
  }, []);

  const fetchMarginalizedAreas = async () => {
    const data = await fetch(`/county/osr`);
    const dataJson = await data.json();
    setOsr(dataJson);
  };

  return (
    <div className="osr-container">
      <h3 className="pt-3"> Revenue Enhancement</h3>

      <p className="text-center">
        The Commission’s efforts in supporting County Governments’ to enhance
        their own source revenue is drawn from the Constitution of Kenya Article
        216. Specifically, the Commission is mandated in Article 216(3b) to
        define and enhance revenue sources of the National and County
        Governments and Article 216(2) requires the commission to also make
        recommendations on matters concerning the financing and financial
        management of County Governments. County Governments draw their
        revenue-raising powers from Article 209 of the Constitution where they
        are empowered to impose property rates and entertainment taxes. In
        addition to these two taxes, County Governments are allowed to raise
        revenue by charging for services they provide to the citizen. The
        Constitution has also provided that County Government can impose other
        taxes provided they are authorized by an Act of Parliament.
      </p>

      <h2 className="text-center">OSR for all counties</h2>

      <Table Columns={OSRColumns} Data={osr} />
    </div>
  );
}
