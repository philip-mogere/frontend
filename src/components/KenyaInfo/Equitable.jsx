import React, { useEffect, useState } from "react";
import { Dropdown } from "react-bootstrap";
import { EquitableColumns } from "./../CountyDetails/Tables/Columns";
import Table from "../CountyDetails/Tables/Table";

export default function Equitable() {
    const [equitable, setEquitable] = useState([]);
    const [year, setYear] = useState("2020/2021");
  
    useEffect(() => {
      fetchMarginalizedAreas();
    }, [year]);
  
    const fetchMarginalizedAreas = async () => {
      const data = await fetch(`/county/equitable?year=${year}`);
      const dataJson = await data.json();
      setEquitable(dataJson);
    };
  

   
    return (
      <>
        <div className="cara">
          <h2 className="text-center">Equitable share allocation for the year {year}</h2>
         
          <Dropdown>
          <Dropdown.Toggle className="mb-3" id="dropdown-basic">
            Select Year
          </Dropdown.Toggle>

          <Dropdown.Menu>
            <Dropdown.Item onClick={() => setYear("2021/2022")}>
              2021/2022
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setYear("2020/2021")}>
              2020/2021
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setYear("2019/2020")}>
              2019/2020
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setYear("2018/2019")}>
              2018/2019
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setYear("2017/2018")}>
              2017/2018
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setYear("2016/2017")}>
              2016/2017
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setYear("2015/2016")}>
              2015/2016
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setYear("2014/2015")}>
              2014/2015
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setYear("2013/2014")}>
              2013/2014
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setYear("2012/2013")}>
              2012/2013
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
          <Table Columns={EquitableColumns} Data={equitable} />
        </div>
      </>
    );
  }
