import React from "react";
import {Button} from 'react-bootstrap'
import "../../App.css";

export default function Pfm() {
  return (
    <div className="pfm text-center">
      <h3 className="mb-1">Public Financial Management</h3>
      <p >
        Article 216(2) mandates the Commission to make recommendations on the
        financing of, and financial management by, County Governments, as
        required by the Constitution and other national legislation. Section
        107(2) (A) of the Public Finance Management Act (PFMA) 2012 requires
        that “Pursuant to Article 201 and 216 of the Constitution and
        notwithstanding subsection (2), the Commission on Revenue Allocation
        shall recommend to the Senate the budgetary ceilings on the recurrent
        expenditures of each county government".
      </p>
     <Button className="text-center" onClick={()=> window.location.href = "/cara"}>Go to Data on PFM</Button>
    </div>
  );
}
