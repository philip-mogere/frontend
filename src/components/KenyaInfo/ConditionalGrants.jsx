import React, { useEffect, useMemo, useState } from "react";
import { useTable, useGlobalFilter } from "react-table";
import GlobalFilter from "./../CountyDetails/Tables/GlobalFilter";
import { Dropdown } from "react-bootstrap";
import { CoditionalGrantsColumns } from './../CountyDetails/Tables/Columns'

export default function CountryAllocations() {
  
  const [allocations, setAllocations] = useState([]);
  const [year, setYear] = useState("2021/2022");

  useEffect(() => {
    fetchCountyAllocations();
  }, [year]);

  const fetchCountyAllocations = async () => {
    const data = await fetch(`/county/allocation?year=${year}`);
    const dataJson = await data.json();
    setAllocations(dataJson);
  };
  const columns = useMemo(() => CoditionalGrantsColumns, []);
  const data = useMemo(() => allocations);

  const tableInstance = useTable(
    {
      columns,
      data,
    },
    useGlobalFilter
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    state,
    setGlobalFilter,
  } = tableInstance;

  const { globalFilter } = state;
  return (
    <>
      <span
        style={{ cursor: "pointer" , color:"white"}}
        onClick={() => (window.location.href = "/")}
      >
        Back to home
      </span>
      <div className="cara">
      <h2 className='text-center'>Grants to the County Governments From the National Government and Other Donors {year}</h2>
      
         
      <Dropdown>
        <Dropdown.Toggle className="mb-3" id="dropdown-basic">
          Select Year
        </Dropdown.Toggle>

        <Dropdown.Menu>
        <Dropdown.Item onClick={() => setYear("2021/2022")}>2021/2022</Dropdown.Item>
        <Dropdown.Item onClick={() => setYear("2020/2021")}>2020/2021</Dropdown.Item>
        <Dropdown.Item onClick={() => setYear("2019/2020")}>2019/2020</Dropdown.Item>
        <Dropdown.Item onClick={() => setYear("2018/2019")}>2018/2019</Dropdown.Item>
        <Dropdown.Item onClick={() => setYear("2017/2018")}>2017/2018</Dropdown.Item>
        <Dropdown.Item onClick={() => setYear("2016/2017")}>2016/2017</Dropdown.Item>
        <Dropdown.Item onClick={() => setYear("2015/2016")}>2015/2016</Dropdown.Item>
        <Dropdown.Item onClick={() => setYear("2014/2015")}>2014/2015</Dropdown.Item>
        <Dropdown.Item onClick={() => setYear("2013/2014")}>2013/2014</Dropdown.Item>
        <Dropdown.Item onClick={() => setYear("2012/2013")}>2012/2013</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
      <GlobalFilter filter={globalFilter} setFilter={setGlobalFilter} />
      <table {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps()}>{column.render("Header")}</th>
              ))}
            </tr>
          ))}
        </thead>

        <tbody {...getTableBodyProps()}>
          {rows.map((row) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map((cell) => {
                  return (
                    <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
      </div>
    </>
  );
}
