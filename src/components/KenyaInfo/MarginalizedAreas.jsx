import React from "react";
import { Tabs, Tab, Card } from "react-bootstrap";
import MarginalizedTable from "../CountyDetails/Tables/MarginalizedTable";
import MarginalizedFirstPolicy from "../CountyDetails/Tables/MarginalizedFirstPolicy";
import MarginalizedThirdPolicy from "./MarginalizedThirdPolicy";

export default function MarginalizedAreas() {
  return (
    <div>
      <Tabs
        fill
        defaultActiveKey="home"
        id="division-of-revenue"
        className="coloredTab"
        style={{ position: "fixed " }}
      >
        <Tab eventKey="home" title="Marginalization" style={{paddingTop:'7rem', paddingLeft:'15rem'}}>
          <div className=" text-center pt-5">
          <Card style={{ width: "60rem", paddingTop:'2rem', paddingBottom:'5rem', border:'solid' }}>
              <Card.Body>
                <Card.Title>Transitional Equalisation</Card.Title>
                <Card.Text>
                  The Constitution in Article 204 provides for an allocation of
                  0.5% of the most recent audited approved account to the
                  Equalisation Fund. The national government shall use the
                  Equalisation Fund only to provide basic services including
                  water, roads, health facilities and electricity to
                  marginalised areas to the extent necessary to bring the
                  quality of those services in those areas to the level
                  generally enjoyed by the rest of the nation, so far as
                  possible. The national government may use the Equalisation
                  Fund either directly, or indirectly through conditional grants
                  to counties in which marginalised communities exist. In the
                  first policy, the national government used the funds directly
                  through the relevant Ministries, Departments and Agencies) to
                  provide services to 14 marginalised counties.
                </Card.Text>
              </Card.Body>
            </Card>
          </div>
        </Tab>
        <Tab eventKey="first" title="1st Policy">
          <MarginalizedFirstPolicy />
        </Tab>
        <Tab eventKey="second" title="2nd Policy">
          <MarginalizedTable />
        </Tab>
        <Tab
          eventKey="third"
          title="3rd Policy"
          style={{ color: "black " }}
        >
            <MarginalizedThirdPolicy/>
        </Tab>
      </Tabs>
    </div>
  );
}
