import "./KenyaInfo.css";
import React from "react";
import {
  Image,
  Accordion,
} from "react-bootstrap";
import CRA_logo from "./CRA_logo.png";
import { Link } from "react-router-dom";

export default function KenyainfoPage() {
  return (
    <div className="mainContainer">
      <Image className="mainPageImage" src={CRA_logo} alt="logo"></Image>
      <h2 className="text-center">Commission on Revenue Allocation</h2>
      <h4 className="text-center">The Commission on Revenue Allocation is established under article 215 of the Kenyan constitution which 
      states that:</h4>
      <div className="mandate">
<ol>
      <li> There is established the Commission on Revenue Allocation.</li>
<li> The Commission shall consist of the following persons appointed by the President--</li>
<ul>
<li style={{listStyle: 'none' }}> a) a chairperson, who shall be nominated by the President and approved by the National Assembly;</li>
<li style={{listStyle: 'none' }}> b) two persons nominated by the political parties represented in the National Assembly according to their proportion of members in the Assembly;</li>
<li style={{listStyle: 'none' }}> c) five persons nominated by the political parties represented in the Senate according to their proportion of members in the Senate; and</li>
<li style={{listStyle: 'none' }}> d) the Principal Secretary in the Ministry responsible for finance.</li>
</ul>
<li> The persons nominated under (2) shall not be members of Parliament.</li>
<li> To be qualified to be a member of the Commission under (2) (a), (b) or (c), a person shall have extensive professional experience in financial and economic matters.</li>
</ol>
</div>
      <h3>Mandate of the commission</h3>
    
      <Accordion defaultActiveKey="0">
        <Accordion.Item eventKey="0">
          <Accordion.Header>Sharing of revenue 
             </Accordion.Header>
          <Accordion.Body>
            CRA makes recommendations on the following:
            <li>
              The basis for equitable sharing of revenue
              raised by national government between national and county
              government, Article 216 (1) (a);
            </li>{" "}
            <li>
              {" "}
              The basis for equitable sharing of revenue
              raised by national government among county governments, Article
              216 (1) (b).{" "}
            </li>{" "}
            <li>
              Any bill that includes provisions dealing with
              sharing of revenue or any financial matter concerning county
              governments, Article 205 (1);{" "}
            </li>{" "}
            
            <hr />
            <span >
            <Link to="/dor">Go to data on Division of revenue</Link>
            </span>
          </Accordion.Body>
        </Accordion.Item>
        <Accordion.Item eventKey="1">
          <Accordion.Header>Marginalization</Accordion.Header>
          <Accordion.Body>
            CRA considers the technical matters of the Commission relating to:
            <li>
              Determination and review of a policy in which the Commission sets
              out the criteria by which to identify the marginalized areas for
              purposes of the Equalization Fund.{" "}
            </li>
            <li>
              Consultations on any Bill that appropriates money out of the
              Equalisation Fund.{" "}
            </li>
            <li>
              {" "}
              Inter-governmental relations between the National and County
              governments as provided for in the constitution and other
              subsidiary laws governing the process, state and reality of
              devolution as a framework for equitable social development and
              national cohesion.{" "}
            </li>
            <li>
              {" "}
              Strategic co-ordination of important stakeholders’ activities with
              National Government, Parliament, civil society, donor community as
              well as other non- governmental actors whose work or mandates have
              a significant bearing and impact on devolution.{" "}
            </li>
            <li>
              Equitable sharing of transfers that are designed to re-distribute
              resources and to promote equalization across counties and
              de-marginalization of hitherto neglected sections of and social
              groups/regions in the Kenyan society.{" "}
            </li>
            <li>
              Research and policy underpinning criteria for the sharing of
              revenues from the Equalisation Fund done by the Commission.{" "}
            </li>
            <li>
              Provision of general direction and guidance in the operation of
              the Stakeholder Management and Marginalization Committee.
            </li>
            <hr />
            <span 
            className="link">
            <Link to="/marginalized">Go to data on Marginalized areas</Link>
            </span>
          </Accordion.Body>
        </Accordion.Item>
        <Accordion.Item eventKey="2">
          <Accordion.Header>Revenue Enhancement</Accordion.Header>
          <Accordion.Body>
            Defines and enhances revenue sources for both the national and
            county governments by;
            <li>
              Work in collaboration with other national government agencies with
              related mandates to ensure enhancement of revenue collected
              nationally.
            </li>
            <li>
              Support counties in the enactment of revenue legislation in line
              with the CoK 2010 and national legislation.
            </li>
            <li>
              Advise county governments on the tax imposition and other revenue
              raising measures as required by the Public Finance Management Act
              (2012).
            </li>
            <li>
              Advice on revenue generation from natural resources for both the
              national and the county governments.
            </li>
            <li>
              Support county governments in setting up and enhancing systems of
              revenue projection, collection, administration and internal
              controls.
            </li>
            <hr/>
            <span>
            <Link to="/enhancements">Go to data on Revenue Enhancement</Link>
            </span>
          </Accordion.Body>
        </Accordion.Item>
        <Accordion.Item eventKey="3">
          <Accordion.Header>Public Finance Management</Accordion.Header>
          <Accordion.Body>
            <li>
              To provide recommendations on the county plans, budgets and budget
              implementation.
            </li>
            <li>
              To make recommendations on the management of recurrent expenditure
              by the county government.
            </li>
            <li>
              To develop a framework for prudent financial management at the
              county level.
            </li>
            <li>
              To promote adherence to the principles of fiscal responsibility
              throughout the PFM structures and processes.
            </li>
            <li>
              To advice both levels of government on adoption and usage of ICT
              systems for prudent financial management.
            </li>

            <hr/>
            <span className="link">
            <Link to="/pfm">Go data on to PFM</Link>
            </span>
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>
    </div>
  );
}
