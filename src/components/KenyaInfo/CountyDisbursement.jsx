import React, { useState, useEffect } from "react";

import { DisbursementColumns } from "./../CountyDetails/Tables/Columns";
import Table from "../CountyDetails/Tables/Table";

export default function CountyDisbursement() {
  const [disbursement, setDisbursement] = useState([]);
  useEffect(() => {
    fetchMarginalizedAreas();
  }, []);

  const fetchMarginalizedAreas = async () => {
    const data = await fetch(`/county/vertical`);
    const dataJson = await data.json();
    setDisbursement(dataJson);
  };

  return (     
      <div >
        <h2 className="text-center pt-5">
          Division of Revenue between the two levels of Government{" "}
        </h2>
        <p className="text-center">
          Vertical Equity Over the period 2013/14 to 2021/22 the national and
          county governments have shared revenues as shown below.
        </p>{" "}
        <h3>
          Sharing of revenue between the national and county governments (Amount
          in Billions of KSH)
        </h3>
        <Table Columns={DisbursementColumns} Data={disbursement} />
      </div>
    
  );
}
