import React, { useEffect, useState } from "react";
import { Dropdown } from "react-bootstrap";
import { PopulationColumns } from "../CountyDetails/Tables/Columns";
import Table from "../CountyDetails/Tables/Table";

export default function CountryPopulation() {
  const [population, setPopulation] = useState([]);
  const [year, setYear] = useState(2009);

  useEffect(() => {
    fetchMarginalizedAreas();
  }, [year]);

  const fetchMarginalizedAreas = async () => {
    const data = await fetch(
      `/county/populations?year=${year}`
    );
    const dataJson = await data.json();
    setPopulation(dataJson);
  };

  return (
    <div>
      <h3 style={{ paddingTop:'2em'}}>Population Breakdown for all counties</h3>
      <h3 style={{ textAlign: "text-center" }}>Financial year: {year}</h3>
      <Dropdown>
        <Dropdown.Toggle className="mb-3" id="dropdown-basic">
          Select Year
        </Dropdown.Toggle>

        <Dropdown.Menu>
          <Dropdown.Item onClick={() => setYear(2009)}>2009</Dropdown.Item>
          <Dropdown.Item onClick={() => setYear(2019)}>2019</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>

      <Table Columns={PopulationColumns} Data={population} />
    </div>
  );
}
