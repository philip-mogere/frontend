import React, { useEffect, useState } from "react";
import Table from "../CountyDetails/Tables/Table";

export default function Municipalities() {
    
    const MunicipalityColumns = [
        {
            Header: "Id",
            accessor: "id",
        },
        {
          Header: "Municipality",
          accessor: "municipality",
        },
        {
            Header: "Population",
            accessor: "population",
        },
        {
          Header: "Status",
          accessor: "status",
        },
        {
            Header: "code",
            accessor: "countyCode",
        }
      ];
    const [municipalities, setMunicipalities] = useState([]);

      useEffect(() => {
        fetchMunicipalities();
      }, []);
    
      const fetchMunicipalities = async () => {
        const data = await fetch('/county/municipality');
        const dataJson = await data.json();
        setMunicipalities(dataJson);
      };

      console.log(municipalities);
      const Data = {
          columns: MunicipalityColumns,
          data: municipalities
      }
    return (
        <div>
            <Table tableData = {Data}/>
        </div>
    )
}
