import React from "react";
import { Tabs, Tab, Card } from "react-bootstrap";
import CountyDisbursement from "./CountyDisbursement";
import HorizontalEquity from "./HorizontalEquity";


export default function DivisionOfRev() {

  return (
    <div>
      <Tabs
        fill
        defaultActiveKey="home"
        id="division-of-revenue"
        className="coloredTab"
        style={{ position: "fixed " }}
      >
        <Tab eventKey="home" title="Division of Revenue">
          <div className="text-center"
          style={{paddingTop:'7rem', paddingLeft:'15rem'}}
          >
            <Card style={{ width: "60rem", paddingTop:'2rem', paddingBottom:'2rem', border:'solid', margin:'auto' }}>
            {/* <Card.Img src='./Equitable revenue sharing.jpeg' /> */}
              <Card.Body>
                <Card.Title>Division of Revenue</Card.Title>
                The principal mandate of the Commission (Article 216 (1)) is to
                make recommendations concerning the criteria for equitable
                sharing of revenues raised nationally, between the national and
                county governments (Vertical Equity) and among the county
                governments (Horizontal Equity)

                <p></p>
            <p> CRA makes recommendations on the following:
            <li>
              The basis for equitable sharing of revenue
              raised by national government between national and county
              government, Article 216 (1) (a);
            </li>{" "}
            <li>
              {" "}
              The basis for equitable sharing of revenue
              raised by national government among county governments, Article
              216 (1) (b).{" "}
            </li>{" "}
            <li>
              Any bill that includes provisions dealing with
              sharing of revenue or any financial matter concerning county
              governments, Article 205 (1);{" "}
            </li>{" "}
            </p>
              </Card.Body>
            </Card>
          </div>
        </Tab>
        <Tab eventKey="Horizontal" title="Horizontal Equity">
          <HorizontalEquity />
        </Tab>
        <Tab
          eventKey="vertical"
          title="Vertical equity"
          style={{ color: "black " }}
        >
          <CountyDisbursement />
        </Tab>
      </Tabs>
    </div>
  );
}
