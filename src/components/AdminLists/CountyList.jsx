import React from 'react'
import { List, Datagrid, TextField, EditButton, DeleteButton } from 'react-admin'

export const CountyList = (props) => {
  return (
    <List {...props}>
        <Datagrid>
            <TextField source='countyCode'/>
            <TextField source='countyName'/>
            <TextField source='address'/>
            <TextField source='phone'/>
            <TextField source='email'/>
            <EditButton basePath='/county'/>
            <DeleteButton basePath='/county'/>
        </Datagrid>
    </List>
  )
}
