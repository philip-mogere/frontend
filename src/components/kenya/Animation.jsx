import { useState, useEffect } from "react";
import { json } from "d3";
import { zoom, geoMercator, geoPath,zoomIdentity, zoomTransform, select, pointer} from "d3";


export default function Animation(){

//   const width = 975;
//   const height = 610;

//   const [usedata, setUseData] = useState({})

//   useEffect(()=>{
//     fetchMap()
// // eslint-disable-next-line react-hooks/exhaustive-deps
//     },[])

//   const zoomFunction = zoom()
//       .scaleExtent([1, 8])
//       .on("zoom", zoomed);

//   const projection  = geoMercator()
//       .scale([3500])
//       .center([30.0, 4.0])
//       .translate([width / 1000, height /6]);
//   const path = geoPath(projection)

//   const svg = select("animationd3").append("svg")
//   .attr("viewBox", [0, 0, width, height])
//   .on("click", reset);

//   const g = svg.append("g");

//   const fetchMap = async() =>json('./kenyan-counties.geojson').then(data=>{
//      setUseData(data.features)   
//   })

  
//   const counties = g.append("g")
//       .attr("fill", "#444")
//       .attr("cursor", "pointer")
//     .selectAll("path")
//     .data(usedata)
//     .join("path")
//       .on("click", clicked)
//       .attr("d", path);

//   g.append("path")
//     .attr("fill", "none")
//     .attr("stroke", "white")
//     .attr("stroke-linejoin", "round")
//     .attr("d", path(counties, (a, b) => a !== b));

//   svg.call(zoomFunction);


//   function reset() {
//     counties.transition().style("fill", null);
//     svg.transition().duration(750).call(
//       zoom.transform,
//       zoomIdentity,
//       zoomTransform(svg.node()).invert([width / 2, height / 2])
//     );
//     }
//   function clicked(event, d) {
//     const [[x0, y0], [x1, y1]] = path.bounds(d);
//     event.stopPropagation();
//     counties.transition().style("fill", null);
//     select(this).transition().style("fill", "red");
//     svg.transition().duration(750).call(
//       zoom.transform,
//         zoomIdentity
//         .translate(width / 2, height / 2)
//         .scale(Math.min(8, 0.9 / Math.max((x1 - x0) / width, (y1 - y0) / height)))
//         .translate(-(x0 + x1) / 2, -(y0 + y1) / 2),
//         pointer(event, svg.node())
//     );
//   }

//   function zoomed(event) {
//     const {transform} = event;
//     g.attr("transform", transform);
//     g.attr("stroke-width", 1 / transform.k);
//   }
//   console.log(usedata)

const [usedata, setUseData] = useState({})
const width = '900'
const height = '700'
const svg = select('.animationd3').append('svg')
               .attr('height', height)
               .attr('width', width)
               .on('click', reset)

const g = svg.append('g')
const projection  = geoMercator()
         .scale([3500])
         .center([30.0, 4.0])
         .translate([width / 1000, height /6]);
const path = geoPath(projection)

const zoomer = zoom()
            .scaleExtent([1, 1.7])
            .on("zoom", zoomed);


useEffect(()=>{
   fetchMap()
// eslint-disable-next-line react-hooks/exhaustive-deps
},[])

const fetchMap = async()=>json('./kenyan-counties.geojson' )
   .then(data => {
      setUseData(data.features) 
         }
   )
   
      svg.selectAll(".place-label")
               .data(usedata)
               .enter().append("text")
               .attr("class", "place-label")
               .attr("transform", function(d) { return "translate(" + path.centroid(d) + ")";})
               .attr("dy", ".35em")
               .attr('text-anchor', 'middle')
               .text(function(d) { return d.properties.COUNTY3_ID; });

       g.selectAll('path')
         .data(usedata)
         .enter()
         .append('path')
         .attr('class',  function(d) { return d.properties.COUNTY; })
         .attr('d',path)
         .attr("cursor", "pointer")
         .attr("stroke", "white")
         .on('click', (e,d)=>{
            clicked(e,d)

            
            select(".countyName")
            .text(d.properties.COUNTY + ' county')
            select(".countyCode")
            .text('County code ' + d.properties.COUNTY3_ID)
            select('.details')
            .style('visibility', "visible")

            svg.selectAll(".place-label")
               .data(usedata)
               .enter().append("text")
               .attr("class", "place-label")
               .attr("transform", function(d) { return "translate(" + path.centroid(d) + ")";})
               .attr("dy", ".35em")
               .attr('text-anchor', 'middle')
               .text(function(d) { return d.properties.COUNTY; });


            svg.selectAll('.place-label')
            .style('visibility', "hidden")
            
            })

         
         
      


      function zoomed(event) {
         const {transform} = event;
         g.attr("transform", transform);
         g.attr("stroke-width", 1 / transform.k);
       }

       function reset() {
         svg.transition().style("fill", null);
         svg.transition().duration(1000).call(
           zoomer.transform,
           zoomIdentity,
           zoomTransform(svg.node()).invert([width / 2, height / 2])
            
         )
         .selectAll('.place-label')
            .style('visibility', "visible");
       }
      

      function clicked(event, d){
         const [[x0, y0], [x1, y1]] = path.bounds(d);
         event.stopPropagation();
         // counties.transition()
         // select(this).transition()
         g.transition().duration(1000).call(
            zoomer.transform, zoomIdentity
            .translate(width / 2, height / 2)
            .scale(Math.min(8, 0.9 / Math.max((x1 - x0) / width, (y1 - y0) / height)))
            .translate(-(x0 + x1) / 2, -(y0 + y1) / 2),
            // pointer(event, svg.node())
            )
      }
  return (
    <div className="animationd3"></div>
    )
}