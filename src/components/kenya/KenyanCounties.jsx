import React from 'react'
import * as d3 from 'd3'
import { useState } from 'react'
import { useEffect } from 'react';
import "./kenya.css"

export const KenyanCounties = () =>{

const [countySvg, setCountySvg] = useState({});

useEffect(()=>{


    const svg = d3.create("svg")
      .attr("../../../public/kenya.svg");

    setCountySvg(svg)
    console.log(svg)
    
},[])


    return (
        <div>
            <svg width="457.63434" height="580.54065">{countySvg}</svg>
        </div>
    )
}
