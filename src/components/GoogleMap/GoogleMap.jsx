import React from 'react'
import GoogleMapReact from 'google-map-react'
import './GoogleMap.css'

const GoogleMap = ({center, zoom}) => {
    
    return (
        <div className="map">
            <GoogleMapReact
                bootstrapURLKeys={{
                    key:'AIzaSyAPA6Zu89xOszNY4FOOdGkE1G70gngaD3A'}}
                defaultCenter={center}
                defaultZoom = { zoom }
            >
            </GoogleMapReact>
        </div>
    )
}
GoogleMap.defaultProps = {
    center : {
        lat: -1.283286, 
        lng: 36.848725
    },
    zoom:6
}


export default GoogleMap
