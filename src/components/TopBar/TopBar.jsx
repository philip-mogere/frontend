import React from "react";
import { Link } from "react-router-dom";
import "./TopBar.css";
import logo from "./logo.png";
// import { BiLogIn } from "react-icons/bi";

export default function TopBar() {
  return (
    <div className="topbar">
      <div className="topbarWrapper">
        <div className="topLeft">
          <Link to="/">
            <img className="logo" src={logo} alt="logo"style={{ paddingBottom:'10px',}} />
          </Link>
          <Link to="/">
          <h4 style={{color:'white', paddingTop:'10px', paddingLeft:'10px'}}>Commission on Revenue Allocation</h4>
          </Link>
        </div>
        <div className="topCenter">
          <h5 style={{ paddingTop:'10px', paddingLeft:'3rem'}}>DATA PORTAL</h5>
        </div>
        <div className="links d-flex gap-5">
          <Link className="links" to="/">
            Home
          </Link>
          <Link className="links" to="/counties">
            Counties
          </Link>
          <Link className="links" to="/about">
            About CRA
          </Link>
          <a className="links" href="https://cra.go.ke/wp-content/uploads/2022/06/Kenya-County-Fact-Sheets-3rd-Edition.pdf">
            County Facts sheet
          </a>
        </div>
        <div className="topRight">
          <div className="topbarIconContainer">
            <Link to="/login" className="link" >
              {/* Login as Admin <BiLogIn /> */}
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
