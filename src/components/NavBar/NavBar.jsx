import React from "react";
import { LinkContainer } from "react-router-bootstrap";
import "./NavBar.css";
import { useState, useEffect } from "react";
import { Container, Nav, NavDropdown } from "react-bootstrap";

export default function NavBar({ county }) {
  var code = county.params.countyCode;
  const [countyName, setCountyName] = useState("");

  useEffect(() => {
    fetchCounty();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fetchCounty = async () => {
    const data = await fetch(`/county/${code}`);
    const dataJson = await data.json();
    setCountyName(dataJson.countyName);
  };

  return (
    <Container>
      <Nav 
      style={{color: 'black'}}
      fill variant="tabs">
        <Nav.Item>
          <LinkContainer to={`/county/${code}/`}>
            <Nav.Link>
              <b>{countyName} County</b>{" "}
            </Nav.Link>
          </LinkContainer>
        </Nav.Item>
          <NavDropdown title="Leadership">
            <LinkContainer to={`/county/${code}/executive`}>
              <NavDropdown.Item eventKey="2.1">County Executive</NavDropdown.Item>
            </LinkContainer>
            <LinkContainer to={`/county/${code}/assembly`}>
              <NavDropdown.Item eventKey="2.2">County Assembly</NavDropdown.Item>
            </LinkContainer>
          </NavDropdown>
          <LinkContainer to={`/county/${code}/budget`}>
            <Nav.Link eventKey="link-3">County Budget</Nav.Link>
          </LinkContainer>
        <Nav.Item>
          <LinkContainer to={`/county/${code}/marginalized`}>
            <Nav.Link eventKey="link-4">Marginalized Areas</Nav.Link>
          </LinkContainer>
        </Nav.Item>
        <Nav.Item>
          <LinkContainer to={`/county/${code}/population`}>
            <Nav.Link eventKey="link-5">Demographic data</Nav.Link>
          </LinkContainer>
        </Nav.Item>
      </Nav>
    </Container>
  );
}
