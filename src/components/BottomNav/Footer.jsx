import React from 'react'
import './footer.css'

export default function Footer() {
    return (
        <div className="footer">
            <div className="footerWrapper">
                <div className="footerContent">
                    <small>Copyright @CRA Kenya 2022</small>
                </div>
            </div>
        </div>
    )
}
