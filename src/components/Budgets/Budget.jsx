import React, { useEffect, useState, useMemo } from "react";
import { useTable, useGlobalFilter } from "react-table";
import { BudgetColumns } from "../CountyDetails/Tables/Columns";
import { Dropdown } from "react-bootstrap";
import GlobalFilter from "../CountyDetails/Tables/GlobalFilter";
import "./Budget.css";

export default function Budget() {
  const [budgets, setBudgets] = useState([]);
  const [year, setYear] = useState("2019/2020");

  useEffect(() => {
    fetchCounties();
  }, [year]);
  const fetchCounties = async () => {
    const data = await fetch(
      `/county/budget?year=${year}`
    );
    const dataJson = await data.json();
    setBudgets(dataJson);
  };

  const columns = useMemo(() => BudgetColumns, []);
  const data = useMemo(() => budgets);

  const tableInstance = useTable(
    {
      columns,
      data,
    },
    useGlobalFilter
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    state,
    setGlobalFilter,
  } = tableInstance;
  const { globalFilter } = state;

  return (
    <div className="budgetpage">
      <h3>County Financial Breakdown for all counties</h3>
      <h3 style={{ textAlign: "text-center" }}>Financial year: {year}</h3>
      <Dropdown>
        <Dropdown.Toggle className="mb-3" id="dropdown-basic">
          Select Year
        </Dropdown.Toggle>

        <Dropdown.Menu>
          <Dropdown.Item onClick={() => setYear("2019/2020")}>
            2019/2020
          </Dropdown.Item>
          <Dropdown.Item onClick={() => setYear("2018/2019")}>
            2018/2019
          </Dropdown.Item>
          <Dropdown.Item onClick={() => setYear("2017/2018")}>
            2017/2018
          </Dropdown.Item>
          <Dropdown.Item onClick={() => setYear("2016/2017")}>
            2016/2017
          </Dropdown.Item>
          <Dropdown.Item onClick={() => setYear("2015/2016")}>
            2015/2016
          </Dropdown.Item>
          <Dropdown.Item onClick={() => setYear("2014/2015")}>
            2014/2015
          </Dropdown.Item>
          <Dropdown.Item onClick={() => setYear("2013/2014")}>
            2013/2014
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
      <GlobalFilter filter={globalFilter} setFilter={setGlobalFilter} />
      <table {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps()}>{column.render("Header")}</th>
              ))}
            </tr>
          ))}
        </thead>

        <tbody {...getTableBodyProps()}>
          {rows.map((row) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map((cell) => {
                  return (
                    <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
